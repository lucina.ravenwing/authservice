using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mime;
using AuthService.Data.DTO;
using AuthService.Data.Models;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using AuthService.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using NLog;
using NLog.Web;

namespace AuthService.Controllers;

/// <summary>
/// Controller used to authenticate users for the *.lucinaravenwing.net domain.
/// </summary>
[ApiController]
[Route("api/auth")]
public class AuthController : Controller {
    #region Class members.
    private readonly Logger _logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

    private readonly IAuthService _authService;
    private readonly IEmailService _emailService;
    #endregion

    #region Ctors
    /// <summary>
    ///     Dependency injected constructor for AuthController class.
    /// </summary>
    /// <param name="authService">Authentication service to do all business logic.</param>
    /// <param name="emailService">Email service to send any emails needed.</param>
    public AuthController(IAuthService authService, IEmailService emailService) {
        _authService = authService;
        _emailService = emailService;
    }
    #endregion

    #region Public class methods.
    /// <summary>
    ///     Signs a user up in the system. Also sends an email to the given email address to confirm given email address.
    /// </summary>
    /// <param name="signUpRequestVm">
    ///     Request to sign a user up.
    /// </param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X POST -H "Content-Type: application/json" -d '{"email": "test@test.com", "username": "testUser", "password": "Password_1", "confirmPassword": "Password_1"}' https://auth.lucinaravenwing.net/api/auth/sign-up
    /// </remarks>
    /// <returns>A notice on successful creation or failure of registration of a user.</returns>
    /// <response code = "200">Indicates the user was successfully signed up.</response>
    /// <response code = "400">Indicates one or more of the inputs led to a failed registration.</response>
    [HttpPost("sign-up")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> SignUp([FromBody] SignUpRequestVm signUpRequestVm) {
        _logger.Debug("Signing a user up.");
        
        // Doing initial validation here in case the user sends in a curl request. Most validation is done in the 
        // front end.
        if (!ModelState.IsValid) {
            return BadRequest("Error: Please provide email, username, and password.");
        }

        if (signUpRequestVm.Password != signUpRequestVm.ConfirmPassword) {
            return BadRequest("Error: Passwords sent in do not match.");
        }

        if (signUpRequestVm.Username.Length < 6 || signUpRequestVm.Username.Contains('@')) {
            return BadRequest("Error: Username must be 6 characters or more and cannot contain @.");
        }

        SignUpDto result = await _authService.SignUpAsync(signUpRequestVm);

        // All messages returned contain error, so if it returns error, return BadRequest.
        if (result.Message.ToLower().Contains("error")) {
            _logger.Error("Error(s): {Result}", result.Message.Replace("Error(s): ", ""));

            return BadRequest(result.Message);
        }

        // Generate a URL to be used in the email directing to the confirm email endpoint in the user controller. 
        string? confirmUrl = _authService.GenerateUrl(Url, "ConfirmEmail", "User",
            new { result.ConfirmToken, email = result.NewUser.Email, origin = signUpRequestVm.RequestOrigin }, "https");

        // If the confirm token is somehow not created, return an internal server error as it wasn't a bad request.
        if (confirmUrl == null) {
            _logger.Error("Confirmation token was not generated.");

            return StatusCode(StatusCodes.Status500InternalServerError, "Error: Confirmation email unable to be sent.");
        }

        _emailService.SendConfirmationEmail(result.NewUser, confirmUrl);

        _logger.Debug("User signed up.");

        return Ok(result.Message);
    }

    /// <summary>
    /// Logs a user into the application.
    /// </summary>
    /// <param name="loginRequestVm">Request containing username/email and password to log in.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X POST https://auth.lucinaravenwing.net/api/auth/login -H "Content-Type: application/json" -d '{"userNameEmail": "test@test.com", "password": "Password_1"}'
    /// </remarks>
    /// <returns>An AuthResponseVm with the user's info.</returns>
    /// <response code = "200">Indicates the user was successfully logged in.</response>
    /// <response code = "400">Indicates one or more of the inputs led to a failed log in or user did not confirm email.</response>
    [HttpPost("log-in")]
    [Produces(MediaTypeNames.Application.Json)]
    [ProducesResponseType(typeof(AuthResponseVm), StatusCodes.Status200OK)]
    public async Task<IActionResult> Login([FromBody] LoginRequestVm loginRequestVm) {
        _logger.Debug("Logging user in.");

        AuthResponseVm result = await _authService.LoginAsync(loginRequestVm);

        // Not sure if this is needed as it should be covered by the required tags, but I'm going to leave it in.
        if (result.Message.Contains("Please enter")) {
            _logger.Debug("User did not enter username or email.");
            return BadRequest(result);
        }

        if (result.Message.Contains("Error: ")) {
            _logger.Debug("User entered incorrect credentials or did not confirm email.");

            return result.Message.Contains("not been confirmed") ? Unauthorized(result) : BadRequest(result);
        }

        _logger.Debug("User logged in.");
        return Ok(result);
    }

    /// <summary>
    /// Logs a user out of the application.
    /// </summary>
    /// <param name="logoutRequestVm">The request containing the refresh token to log out.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X POST https://auth.lucinaravenwing.net/api/auth/log-out -H "Content-Type: application/json" -d '{"RefreshToken": "xXk24jnn5inxWXPUAD57xUufYeB7cIwY+yXS4fLE4QfPiwhRfaSIDG780nL9HwD+hZWlwkyobyExPmzj0FNMlOcXsYKi2Rtg5YV17BC8su7jdxC8Ky4rYb1q27kGOP/SPVYUPzcwxmWm29WOYNsdLK1xrBuXqOpaTTJrI8Yzr7isoLbV5N6eYo1WrBV3TvhvaIEaEl4ObzzDOHMKy2ogw4l1wydVx7Uv6cg/N6qRMbP/d15/JVETl0+YEC4wIc3voPtt+JCy05RN4R5PNl4sYdk4Z6UABUVZEtdAlYUkCglWYPUrYwJeq4xTkIGgxqy5shyLi5jnqqfWxzop/NWayOX+M3lrtDEsO4vgHzDWMkiPgm4s0EsHoY0C93pjk85ZJcFQuSSNZd5EnfCwTzI5bvwsMskKyNHoM2xDwqEyQZV88QSZyLgr31zQn6F8P4sXv/zKmq6X/sY2pkYEiEKL7rM+PbEeBKrkNcR7jty4vH6yvmKqtINJCeh83w8XaobLfJ6IxLTnsWjJneIsCpZZQv991wMWw9YcW4OHAAZHVkCrq41F/VlRwByA0tiFw8I+KchaJV9zBCUDLwkpzLLTTj6tYhJpS5E+asoSzyUjfBzmkGh54g8KtGLmJoHS8a0bXglGpJaT0FKqXuyskEugDdwb2Gn4LplCXGGEfDPiobw="}'
    /// </remarks>
    /// <returns>A string denoting the user was successfully logged out.</returns>
    /// <response code = "200">Indicates the user was successfully logged out.</response>
    [HttpPost("log-out")]
    [Produces(MediaTypeNames.Application.Json)]
    // To avoid the many typo warnings in the refresh key.
    [SuppressMessage("ReSharper", "CommentTypo")]
    public IActionResult Logout([FromBody] LogoutRequestVm logoutRequestVm) {
        _logger.Debug("Logging user out.");

        if (!ModelState.IsValid || logoutRequestVm.RefreshToken == "") {
            return BadRequest("Please provide refresh token.");
        }

        // No return type because the user doesn't need to care if there was anything actually done in the backend for
        // this one. The tokens will be deleted from the cache on their end and will be overwritten in the database
        // when they log back in if an error occurs.
        _authService.LogoutAsync(logoutRequestVm);

        _logger.Debug("User logged out successfully.");
        return Ok("User logged out successfully.");
    }

    /// <summary>Sends an email with a reset code to a user given their email or username.</summary>
    /// <param name="forgotPasswordRequestVm">Request for username or email of user to send email.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X POST -H "Content-Type: application/json" -d '{"userNameEmail": "test@test.com"}' https://auth.lucinaravenwing.net/api/forgot-password
    /// </remarks>
    /// <returns>A notice on whether the email was successfully sent or not.</returns>
    /// <response code = "200">Indicates the email was sent successfully.</response>
    /// <response code = "400">Indicates either missing inputs or that the user doesn't exist.</response>
    [HttpPost("forgot-password")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordRequestVm forgotPasswordRequestVm) {
        _logger.Debug("User requested forgot password endpoint.");
        // Returns a tuple with the message and a user. User is new AuthUser() if no user is found.
        Tuple<string, AuthUser> result = await _authService.ForgotPasswordAsync(forgotPasswordRequestVm);

        if (result.Item1.Contains("Error: ")) {
            return BadRequest(result);
        }

        string? userName = result.Item2.UserName;
        string? email = result.Item2.Email;

        // generates a URL for use in the email that links to the request password reset endpoint in the user controller. 
        string? tokenUrl = _authService.GenerateUrl(Url, "RequestPasswordReset", "User",
            new { Token = result.Item1, Email = email, forgotPasswordRequestVm.Origin }, protocol: "https");

        // I don't know how this would happen, but if it's null, return a server error.
        if (tokenUrl == null) {
            _logger.Error("Email was not sent.");
            return StatusCode(StatusCodes.Status500InternalServerError, "Error: Password reset email was not sent.");
        }

        _emailService.SendPasswordResetEmail(userName, email, tokenUrl);

        _logger.Debug("Email sent successfully.");

        return Ok("Password reset email sent.");
    }

    /// <summary>Endpoint to refresh access token.</summary>
    /// <param name="tokenRequestVm">Request sent to create new access token.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X POST -H "Content-Type: application/json" -d '{"token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg", "refreshToken": "dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0="}' https://auth.lucinaravenwing.net/api/refresh-token
    /// </remarks>
    /// <returns>A result on whether the token was refreshed.</returns>
    /// <response code = "200">Indicates the token was refreshed successfully.</response>
    /// <response code = "400">Indicates either missing inputs or that the user doesn't exist.</response>
    [HttpPost("refresh-token")]
    [Produces(MediaTypeNames.Application.Json)]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public async Task<IActionResult> RefreshToken([FromBody] TokenRequestVm tokenRequestVm) {
        if (!ModelState.IsValid) {
            return BadRequest("Please provide access and refresh tokens.");
        }

        AuthResponseVm result = await _authService.ValidateAndGenerateTokenAsync(new JwtSecurityTokenHandler(), tokenRequestVm);

        if (result.Message.Contains("Error: ")) {
            _logger.Debug("Inproper input.");

            return BadRequest(result);
        }

        return Ok(result);
    }
    #endregion
}