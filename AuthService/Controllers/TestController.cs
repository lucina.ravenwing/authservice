using System.Diagnostics.CodeAnalysis;
using AuthService.Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NLog;
using NLog.Web;

namespace AuthService.Controllers;

/// <summary>Controller used to test a user's access.</summary>
[ApiController]
[Route("api/test")]
public class TestController : Controller {
    #region Class members.
    private readonly Logger _logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
    #endregion

    #region Public class methods.
    /// <summary>Public API that everyone can access.</summary>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -H "Content-Type: application/json" https://auth.lucinaravenwing.net/api/test/public
    /// </remarks>
    /// <returns>String saying that you have accessed the public API.</returns>
    /// <response code="200">Properly able to access the Public API.</response>
    [HttpGet("public")]
    public IActionResult TestPublic() {
        _logger.Info("Someone found the test controller.");

        return Ok("You have reached the public Test API.\n");
    }

    /// <summary>User API that only logged in users with User role can access. Every user should have User role.</summary>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" https://auth.lucinaravenwing.net/api/test/user
    /// </remarks>
    /// <returns>String saying that you have accessed the User API.</returns>
    /// <response code="200">Properly able to access the User API.</response>
    /// <response code="401">User lacks proper access.</response>
    [HttpGet("user")]
    [Authorize(Roles = UserRoles.User)]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public IActionResult TestUser() {
        _logger.Info("Someone found the test controller.");


        return Ok("You have reached the user Test API.\n");
    }

    /// <summary>Moderator API that only logged in users with Moderator role can access.</summary>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" https://auth.lucinaravenwing.net/api/test/moderator
    /// </remarks>
    /// <returns>String saying that you have accessed the Moderator API.</returns>
    /// <response code="200">Properly able to access the Moderator API.</response>
    /// <response code="401">User lacks proper access.</response>
    [HttpGet("moderator")]
    [Authorize(Roles = UserRoles.Moderator)]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public IActionResult TestModerator() {
        _logger.Info("Someone found the test controller.");


        return Ok("You have reached the moderator Test API.\n");
    }

    /// <summary>Admin API that only logged in users with Admin role can access.</summary>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" https://auth.lucinaravenwing.net/api/test/admin
    /// </remarks>
    /// <returns>String saying that you have accessed the Admin API.</returns>
    /// <response code="200">Properly able to access the Admin API.</response>
    /// <response code="401">Admin lacks proper access.</response>
    [HttpGet("admin")]
    [Authorize(Roles = UserRoles.Admin)]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public IActionResult TestAdmin() {
        return Ok("You have reached the admin Test API.\n");
    }
    #endregion
}