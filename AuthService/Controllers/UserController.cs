using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mime;
using System.Web;
using AuthService.Data.Models;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using AuthService.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NLog;
using NLog.Web;

namespace AuthService.Controllers;

/// <summary>A Controller used to handle user-related actions not related to authentication.</summary>
[ApiController]
[Route("api/user")]
public class UserController : Controller {
    #region Class members.
    private readonly Logger _logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

    private readonly IUserService _userService;
    private readonly IEmailService _emailService;

    private readonly string? _key;
    #endregion

    #region Ctors
    /// <summary>Injected constructor for User Controller.</summary>
    /// <param name="userService">Service to handle user related actions.</param>
    /// <param name="emailService">Service to send emails.</param>
    /// <param name="configuration">The configuration of the application.</param>
    public UserController(IUserService userService, IEmailService emailService, IConfiguration configuration) {
        _userService = userService;
        _emailService = emailService;

        _key = configuration.GetSection("Misc:get-user-data-key").Value;
    }
    #endregion

    #region Public class methods.
    /// <summary>Confirms a user's email.</summary>
    /// <param name="emailConfirmationRequestVm">The request containing the confirmation token and the user's email for verification.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X GET -H "Content-Type: application/json" https://auth.lucinaravenwing.net/api/user/confirm-email?token=CfDJ8BrK8kI16thIvEKAXiaCocMKIEMGekg0qrucCyLjJtYJGpQaJx1KNn7%2Fg3iFtc0XQ6WdbchwGA5X66UvEmpdZPm0iqUCp2VRS3AprtA%2BgRY2wctVxPuu9Nsj8FgK2ANpJVop5EQ57Agc5zQIkGn70YLxOnakc4QuM%2Fei3RdCPa53FHJIulQ0z4gDHr37Fhf3aA%3D%3D&amp;email=test@test.com
    /// </remarks>
    /// <returns>A string denoting the user was successfully logged out.</returns>
    /// <response code = "200">Indicates the user was successfully logged out.</response>
    [HttpGet("confirm-email")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> ConfirmEmail([FromQuery] EmailConfirmationRequestVm emailConfirmationRequestVm) {
        _logger.Debug("Email confirmation started.");

        string result = await _userService.ConfirmEmail(emailConfirmationRequestVm);

        // This error should not occur, so we're asking them to send an email to get it figured out.
        if (result.Contains("Error")) {
            return BadRequest(result + " Please send an email to lucinaravenwing.net@gmail.com with this error.");
        }

        _logger.Debug("Email confirmation completed.");

        // The redirect to send the user back to the original website they were using when the request started.
        string redirect = emailConfirmationRequestVm.Origin;

        // If it's blank, just return a result, else redirect them back to the websites email confirmed page.
        if (redirect.Equals("")) {
            return Ok(result);
        }

        return Redirect(redirect + "/email-confirmed");
    }

    /// <summary>Changes a user's password. An email notifying the user of the change is also sent.</summary>
    /// <param name="passwordUpdateRequestVm">Request containing current password and updated password.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" -d '{"currentPassword":"Password_1", "newPassword":"Password_2"}' https://auth.lucinaravenwing.net/api/user/change-password
    /// </remarks>
    /// <returns>Response denoting that the email was changed along with new tokens.</returns>
    /// <response code = "200">Indicates the user's password was successfully changed.</response>
    /// <response code = "400">Indicates the user entered incorrect data.</response>
    /// <response code = "401">Indicates either the user is not logged in or has insufficient privileges.</response>
    [HttpPost("change-password")]
    [Authorize]
    [Produces(MediaTypeNames.Application.Json)]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public async Task<IActionResult> ChangePassword([FromBody] PasswordUpdateRequestVm passwordUpdateRequestVm) {
        _logger.Debug("User requested password change.");

        if (passwordUpdateRequestVm.NewPassword != passwordUpdateRequestVm.ConfirmNewPassword) {
            return BadRequest("Error: Updated passwords do not match.");
        }

        EmailAuthResponseVm result = await _userService.ChangePassword(passwordUpdateRequestVm, passwordUpdateRequestVm.UserId);

        if (result.AuthResponse == null || result.Email == null || result.Username == null) {
            _logger.Error("Error: Email auth response returned invalid data.");

            return StatusCode(StatusCodes.Status500InternalServerError, "Error: Auth returned improper result.");
        }

        if (result.AuthResponse.Message.Contains("Error")) {
            _logger.Error("Email auth response returned invalid data.");

            return BadRequest(result.AuthResponse);
        }

        string? username = result.Username;
        string? email = result.Email;

        // send a notification to the user that their password was updated.
        _emailService.SendPasswordChangeNoticeEmail(username, email);
        _logger.Debug("Notification sent.");

        _logger.Debug("Password change completed.");
        return Ok(result.AuthResponse);
    }

    /// <summary>Changes a user's username. An email notifying the user of the change is also sent.</summary>
    /// <param name="usernameUpdateRequestVm">Request containing the updated username.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg" -d '{"updatedUsername":"testUser2"}' https://auth.lucinaravenwing.net/api/user/change-username
    /// </remarks>
    /// <returns>Response denoting that the username was changed along with new tokens.</returns>
    /// <response code = "200">Indicates the user's username was successfully changed.</response>
    /// <response code = "400">Indicates the user entered incorrect data.</response>
    /// <response code = "401">Indicates either the user is not logged in or has insufficient privileges.</response>
    [HttpPost("change-username")]
    [Authorize]
    [Produces(MediaTypeNames.Application.Json)]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public async Task<IActionResult> ChangeUserName([FromBody] UsernameUpdateRequestVm usernameUpdateRequestVm) {
        _logger.Debug("User requested username update.");

        if (usernameUpdateRequestVm.UpdatedUsername.Length < 6 ||
            usernameUpdateRequestVm.UpdatedUsername.Contains('@')) {
            return BadRequest("Error: Username must be 6 or more characters and not contain @ symbol.");
        }

        EmailAuthResponseVm result = await _userService.ChangeUserName(usernameUpdateRequestVm);

        if (result.AuthResponse == null || result.Email == null || result.Username == null) {
            _logger.Error("Email auth response returned invalid data.");

            return StatusCode(StatusCodes.Status500InternalServerError, "Error: Result returned incomplete data.");
        }

        if (result.AuthResponse.Message.Contains("Error")) {
            return BadRequest(result.AuthResponse);
        }

        // Get the original name in case the user didn't change their name. This will be used in the email.
        string? originalUsername = result.Username;
        string? email = result.Email;

        // Send the user an email notifying that their username has been updated.
        _emailService.SendUsernameChangeNoticeEmail(originalUsername, usernameUpdateRequestVm.UpdatedUsername, email);

        _logger.Debug("Username updated and email sent.");

        return Ok(result.AuthResponse);
    }

    /// <summary>
    /// Redirect to the calling application to get input from user with the new password.
    /// </summary>
    /// <param name="redirectPasswordRequestVm">The request with all the pertinent information.</param>
    /// <returns></returns>
    [HttpGet("request-password-reset")]
    [Produces(MediaTypeNames.Application.Json)]
    public IActionResult RequestPasswordReset(
        [FromQuery] RedirectPasswordRequestVm redirectPasswordRequestVm) {
        // Require an origin from the request.
        if (redirectPasswordRequestVm.Origin == "") {
            return BadRequest("Please enter an origin.");
        }

        string token = HttpUtility.UrlEncode(redirectPasswordRequestVm.Token);

        return Redirect(redirectPasswordRequestVm.Origin + "/reset-password?Email=" +
                        redirectPasswordRequestVm.Email + "&Token=" + token);
    }

    /// <summary>Resets a user's password. Sends an email notification.</summary>
    /// <param name="passwordResetRequestVm">Request containing password reset token, updated password, and email to pull user data.
    /// An email notifying the user of the change is also sent.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X GET -H "Content-Type: application/json" -d '{"token": "CfDJ8BrK8kI16thIvEKAXiaCocP7VZcTBf7QapAR2j%2BEEY47ocqULpsJSgY8wnd7xgBfpVrsHwZ83T968rEnTXxF2DRZfOdPSZhN5sZ861HXTRPf0k2cihxytse%2BTg7Y6A8M12HqpPAMpBxpVvinCLtM9O8mZs%2Bb0xPFEms6J%2FOgMUTuUMLDlMEJmY%2FNRCo6AKJWDg%3D%3D", "newPassword":"Password_2", "email": "test@test.com, "origin": "https://ygo.lucinaravenwing.net"}' https://auth.lucinaravenwing.net/api/user/reset-password
    /// </remarks>
    /// <returns>Response denoting that the password was reset along with new tokens.</returns>
    /// <response code = "200">Indicates the user's password was successfully reset.</response>
    /// <response code = "400">Indicates an input was incorrect.</response>
    [HttpPost("reset-password")]
    [Produces(MediaTypeNames.Application.Json)]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public async Task<IActionResult> ResetPassword(PasswordResetRequestVm passwordResetRequestVm) {
        AuthResponseVm result = await _userService.ResetPassword(passwordResetRequestVm.Email,
            passwordResetRequestVm.NewPassword, passwordResetRequestVm.Token);

        if (result.Message.Contains("Error:")) {
            return BadRequest(result);
        }

        // Send email notifying user that password has changed.
        _emailService.SendPasswordChangeNoticeEmail(GetUserNameFromTokenParam(result.Token),
            GetEmailFromTokenParam(result.Token));

        return Ok(result);
    }

    /// <summary>Request to resend the confirmation email to a given user's email.</summary>
    /// <param name="resendConfirmEmailRequestVm">Request with information needed to resend user confirmation email.</param>
    /// <remarks>
    /// Sample Curl Request:
    ///
    ///     curl -X POST -H "Content-Type: application/json" -d '{"email": "test@test.com}' https://auth.lucinaravenwing.net/api/user/resend-confirm-email
    /// </remarks>
    /// <returns>A string denoting success or failure of email send.</returns>
    /// <response code = "200">Indicates the email was successfully sent to the user.</response>
    /// <response code = "400">Indicates an input was incorrect.</response>
    [HttpPost("resend-confirm-email")]
    [Produces(MediaTypeNames.Application.Json)]
    public async Task<IActionResult> ResendConfirmationEmail(
        [FromBody] ResendConfirmEmailRequestVm resendConfirmEmailRequestVm) {
        Tuple<string, AuthUser> result = await _userService.ResendConfirmEmail(resendConfirmEmailRequestVm);

        if (result.Item1.Contains("Error: ")) {
            return BadRequest("Unable to resend email confirmation. Reason(s): " + result.Item1);
        }

        // Create a URL directing to the confirm email endpoint in the user controller.
        string? confirmUrl = _userService.GenerateUrl(Url, "ConfirmEmail", "User",
            new {
                ConfirmToken = result.Item1, email = result.Item2.Email,
                origin = resendConfirmEmailRequestVm.RequestOrigin
            }, "https");

        // If the confirm token is somehow not created, return an internal server error as it wasn't a bad request.
        if (confirmUrl == null) {
            _logger.Error("Confirmation token was not generated.");

            return StatusCode(StatusCodes.Status500InternalServerError, "Error: Confirmation email unable to be sent.");
        }

        _emailService.SendConfirmationEmail(result.Item2, confirmUrl);

        return Ok("Confirmation email resent successfully.");
    }

    /// <summary>
    /// Controller method used to get user data for applications that have not had that user log in previously.
    /// </summary>
    /// <returns>Result with a message saying the success or failure and the user data if successful.</returns>
    [HttpPost("get-user-data")]
    [Produces(MediaTypeNames.Application.Json)]
    [ApiExplorerSettings(IgnoreApi = true)]
    public async Task<IActionResult> GetUserData([FromBody] UserDataRequestVm userDataRequestVm) {
        // This is for internal use only, so we are using a key to call into the endpoint.
        if (userDataRequestVm.Key != _key) {
            return BadRequest(new UserDataReturnVm() {
                Message = "Key is incorrect.",
                Username = "",
                Email = ""
            });
        }

        if (userDataRequestVm is { Email: "", Username: "" }) {
            return BadRequest(new UserDataReturnVm() {
                Message = "Please send either a username or email to search.",
                Username = "",
                Email = ""
            });
        }

        UserDataReturnVm userData = await _userService.GetUserData(userDataRequestVm);

        if (userData.Message == "" || !userData.Message.Contains("Error") && userData.Username == ""
                                   || !userData.Message.Contains("Error") && userData.Email == "") {
            return StatusCode(StatusCodes.Status500InternalServerError, userData);
        }

        return Ok(userData);
    }
    #endregion

    #region Private class methods.
    /// <summary>Helper method to pull username from provided access token.</summary>
    /// <param name="token">Access token to pull info from.</param>
    /// <returns>Username from provided access token.</returns>
    private static string GetUserNameFromTokenParam(string token) {
        JwtSecurityToken tokenDecode = new JwtSecurityTokenHandler().ReadJwtToken(token);

        return tokenDecode.Claims.First(claimQuery => claimQuery.Type == "sub").Value.ToString();
    }

    /// <summary>Helper method to pull email from provided access token.</summary>
    /// <param name="token">Access token to pull info from.</param>
    /// <returns>Email from provided access token.</returns>
    private static string GetEmailFromTokenParam(string token) {
        JwtSecurityToken tokenDecode = new JwtSecurityTokenHandler().ReadJwtToken(token);

        return tokenDecode.Claims.First(claimQuery => claimQuery.Type == "email").Value.ToString();
    }
    #endregion
}