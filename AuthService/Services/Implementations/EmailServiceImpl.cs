using System.Net;
using System.Net.Mail;
using AuthService.Data.Models;
using AuthService.Services.Interfaces;
using NLog;
using NLog.Web;

namespace AuthService.Services.Implementations; 

/// <summary>Implementation of EmailService business logic.</summary>
public class EmailServiceImpl : IEmailService {
    
    #region Class members.
    private readonly Logger _logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

    private readonly SmtpClient _smtpClient;
    private readonly MailAddress _fromAddress;
    #endregion

    #region Ctors
    /// <summary>
    /// Dependency injected constructor for the EmailServiceImpl class. Also initializes some variables required to send emails.
    /// </summary>
    /// <param name="configuration">The configuration for the project. Allows pulling data from app settings.</param>
    public EmailServiceImpl(IConfiguration configuration) {
        // Set up the gmail account to send the emails.
        _fromAddress = new MailAddress("lucinaravenwing.net@gmail.com", "LucinaRavenwing.net");
        string? gmailPassword = configuration.GetSection("Misc:gmail-password").Value;

        // Set up the client for sending emails.
        _smtpClient = new SmtpClient() {
            Host = "smtp.gmail.com",
            Port = 587,
            EnableSsl = true,
            DeliveryMethod = SmtpDeliveryMethod.Network,
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential(_fromAddress.Address, gmailPassword)
        };
    }
    #endregion

    #region Public class methods.
    /// <summary>
    /// Implementation of the SendConfirmationEmail method.
    /// Sends an email containing the information a user needs to confirm their email.
    /// </summary>
    /// <param name="user">The user that signed up and will be receiving the email.</param>
    /// <param name="confirmTokenUrl">The confirmation token generated by the application.</param>
    public void SendConfirmationEmail(AuthUser user, string confirmTokenUrl) {
        _logger.Debug("Sending confirmation email.");
        
        MailAddress toAddress = new(user.Email ?? "lucinaravenwing.net@gmail.com", user.UserName);

        // Set up the message that will be sent.
        using MailMessage message = new(_fromAddress, toAddress);
        message.Subject = "Please verify your e-mail for lucinaravenwing.net.";
        message.Body = "Welcome, " + user.UserName + ".<br><br>Click the link below to confirm your " +
                       "email address.<br><br><a href=" + confirmTokenUrl + ">Click here.</a><br><br><i>This link will " +
                       "expire in 24 hours.</i>";
        message.IsBodyHtml = true;
        _smtpClient.Send(message);

        _logger.Debug("Confirmation email sent.");
    }

    /// <summary>
    /// Implementation of the SendPasswordChangeNoticeEmail.
    /// Sends a notification to the user informing them that their password has been updated.
    /// </summary>
    /// <param name="userName">The username of the user whose password was changed.</param>
    /// <param name="email">The email address of the user whose password was changed.</param>
    public void SendPasswordChangeNoticeEmail(string userName, string email) {
        _logger.Debug("Sending password change notice email.");
        
        MailAddress toAddress = new(email, userName);
        
        // Set up the message to send to the user.
        using MailMessage message = new(_fromAddress, toAddress);
        message.Subject = "Password change notification for lucinaravenwing.net.";
        message.Body = "Hello " + userName + ". This email is being sent to inform you about a password change that" +
                       "has occurred on your account.<br><br>If you have made this change, you may ignore this email." +
                       "<br><br>If you did not change your password, please reply to this email for assistance.";
        message.IsBodyHtml = true;

        _smtpClient.Send(message);

        _logger.Debug("Password change notice email sent.");
    }

    /// <summary>
    /// Implementation of the SendUsernameChangeNoticeEmail.
    /// Sends a notification to the user informing them that their username was changed.
    /// </summary>
    /// <param name="originalUserName">The original username of the user whose username was changed.</param>
    /// <param name="updatedUserName">The updated username of the user whose username was changed.</param>
    /// <param name="email">The email address of the user whose username was changed.</param>
    public void SendUsernameChangeNoticeEmail(string originalUserName, string updatedUserName, string email) {
        _logger.Debug("Sending username change notice.");
        
        MailAddress toAddress = new(email, originalUserName + "/" + updatedUserName);
        
        // Set up the message to send to the user.
        using MailMessage message = new(_fromAddress, toAddress);
        message.Subject = "Username change notification for lucinaravenwing.net.";
        message.Body = "Hello " + updatedUserName + "/" + originalUserName + ". This email is being sent to inform you about " +
                       "a username change that has occurred on your account." +
                       "<br><br>If you have made this change, you may ignore this email." +
                       "<br><br>If you did not change your username, please reply to this email for assistance.";
        message.IsBodyHtml = true;

        _smtpClient.Send(message);

        _logger.Debug("Username change notice sent.");
    }

    /// <summary>
    /// Implementation of the SendPasswordResetEmail.
    /// Sends a message containing information on how to reset a user's password.
    /// </summary>
    /// <param name="userName">The username of the user that requested the password reset.</param>
    /// <param name="email">The email address of the user that requested the password reset.</param>
    /// <param name="resetTokenUrl">The reset token that was generated by the application.</param>
    public void SendPasswordResetEmail(string? userName, string? email, string resetTokenUrl) {
        _logger.Debug("Sending password reset email.");

        if (email != null) {
            MailAddress toAddress = new(email, email);
        
            // Set up the message to send to the user.
            using MailMessage message = new(_fromAddress, toAddress);
            message.Subject = "Password reset for lucinaravenwing.net.";
            message.Body = "Hello " + userName + ". " +
                           "<br><br>Click the link below to reset your " +
                           "password.<br><br><a href=" + resetTokenUrl + ">Click here.</a><br><br><i>This link will " +
                           "expire in 24 hours.</i>";
            message.IsBodyHtml = true;

            _smtpClient.Send(message);
            _logger.Debug("Password reset email sent.");
        }

        _logger.Error("Password reset email failed to send.");
    }
    #endregion
}