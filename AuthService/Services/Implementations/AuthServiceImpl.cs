using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using AuthService.Data.DTO;
using AuthService.Data.Models;
using AuthService.Data.Repos;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using AuthService.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NLog;
using NLog.Web;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace AuthService.Services.Implementations;

/// <summary>Implementation of AuthService business logic.</summary>
[SuppressMessage("ReSharper", "InconsistentlySynchronizedField")]
public class AuthServiceImpl : IAuthService {
    #region Class members.
    private readonly Logger _logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

    private readonly IAuthUserRepo _authUserRepo;
    private readonly IRefreshTokenRepo _tokenRepo;
    private readonly IConfiguration _configuration;
    private readonly TokenValidationParameters _tokenValidationParameters;
    private readonly UserManager<AuthUser> _userManager;
    #endregion

    #region Ctors
    /// <summary>Dependency injected constructor for the implementation of Auth Service.</summary>
    /// <param name="authUserRepo">Repository to access and update User information.</param>
    /// <param name="tokenRepo">Entity framework portion exposed for RefreshToken.</param>
    /// <param name="configuration">Top level configuration details to pull info out of app settings files.</param>
    /// <param name="tokenValidationParameters">Validation options configured for tokens.</param>
    /// <param name="userManager">Repository to access and update User information.</param>
    public AuthServiceImpl(IAuthUserRepo authUserRepo, IRefreshTokenRepo tokenRepo,
        IConfiguration configuration, TokenValidationParameters tokenValidationParameters,
        UserManager<AuthUser> userManager) {
        _authUserRepo = authUserRepo;
        _tokenRepo = tokenRepo;
        _configuration = configuration;
        _tokenValidationParameters = tokenValidationParameters;
        _userManager = userManager;
    }
    #endregion

    #region Public class methods.
    /// <summary>
    /// Implementation of SignUpAsync method.
    /// This method will create a user if all information is correct and doesn't overlap with existing records.
    /// </summary>
    /// <param name="signUpRequestVm">Request with information needed to sign a User up.</param>
    /// <returns>Data transfer object with return message, email confirmation token, and new user info.</returns>
    public async Task<SignUpDto> SignUpAsync(SignUpRequestVm signUpRequestVm) {
        PasswordValidator<AuthUser> validator = new();
        IdentityResult valid = await validator.ValidateAsync(_userManager, null!, signUpRequestVm.Password);

        // If the password information isn't valid, return an errored SignUpDto to the user.
        if (!valid.Succeeded) {
            string outputErrors = valid.Errors.Aggregate("", (current, error) => current + (error.Description + ", "));

            return new SignUpDto() {
                Message = "Error: Password does not meet requirements. Error(s): " + outputErrors
            };
        }

        // Check if email and username exist then return errors if they already exist.
        AuthUser? emailExists = await _authUserRepo.FindByEmailAsync(signUpRequestVm.EmailAddress);
        AuthUser? usernameExists = await _authUserRepo.FindByUsernameAsync(signUpRequestVm.Username);

        // Set up the string with the errors to send to the user.
        if (emailExists != null || usernameExists != null) {
            string errors = "Error(s): ";
            if (emailExists != null) {
                errors += "A user with given email already exists.";
            }

            if (usernameExists != null) {
                if (emailExists != null) {
                    errors += " ";
                }

                errors += "A user with given username already exists.";
            }

            return new SignUpDto() {
                Message = errors
            };
        }

        AuthUser newUser = new() {
            Email = signUpRequestVm.EmailAddress,
            UserName = signUpRequestVm.Username,
            SecurityStamp = Guid.NewGuid().ToString()
        };

        IdentityResult result = await _authUserRepo.CreateAsync(newUser, signUpRequestVm.Password);

        if (!result.Succeeded) {
            string outputErrors = result.Errors.Aggregate("", (current, error) => current + (error.Description + ", "));

            _logger.Error("User not created for uncaught reason. Errors given: {}", outputErrors);

            outputErrors = outputErrors.Substring(0, outputErrors.Length - 2);
            return new SignUpDto() {
                Message = $"Error: User was not able to be created. Errors: {outputErrors}"
            };
        }

        // Only create users with the User role. Mod/Admin will get manually added.
        await _authUserRepo.AddToRoleAsync(newUser, UserRoles.User);

        // Confirm email send.
        _logger.Debug("Sending email to newly signed up user.");
        string confirmToken = await _authUserRepo.GenerateEmailConfirmationTokenAsync(newUser);

        return new SignUpDto() {
            Message = "User created successfully!",
            NewUser = newUser,
            ConfirmToken = confirmToken
        };
    }

    /// <summary>
    /// Implementation of LoginAsync method.
    /// This method logs a user into the system based on either their username or email and password.
    /// </summary>
    /// <param name="loginVm">Request with information needed to log a user in.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    public async Task<AuthResponseVm> LoginAsync(LoginRequestVm loginVm) {
        if (loginVm.UserNameEmail == "") {
            return new AuthResponseVm() { Message = "Error: Please enter email or username." };
        }

        // If the UserNameEmail field contains an at sign, then it is an email.
        AuthUser? loginUser = loginVm.UserNameEmail.Contains('@')
            ? await _authUserRepo.FindByEmailAsync(loginVm.UserNameEmail)
            : await _authUserRepo.FindByUsernameAsync(loginVm.UserNameEmail);

        if (loginUser != null) {
            // If the user is locked out.
            if (loginUser.LockoutEnd > DateTime.UtcNow) {
                return new AuthResponseVm() {
                    Message =
                        "Error: User has attempted to log in too many times. Please wait up to 15 minutes and try again."
                };
            }

            // System requires user to confirm email before they can log in, so this is the error.
            if (!await _authUserRepo.IsEmailConfirmedAsync(loginUser)) {
                await _authUserRepo.AccessFailedAsync(loginUser);

                return new AuthResponseVm() {
                    Message = "Error: Email has not been confirmed for this account."
                };
            }

            if (await _authUserRepo.CheckPasswordAsync(loginUser, loginVm.Password)) {
                // If a user successfully logs in, reset the failed attempt count.
                await _authUserRepo.ResetAccessFailedCountAsync(loginUser);

                return await GenerateJwtAsync(loginUser, null);
            }

            // Increment failed attempt on unsuccessful login.
            await _authUserRepo.AccessFailedAsync(loginUser);

            return new AuthResponseVm() {
                Message = "Error: Username, email, or password is incorrect."
            };
        }

        _logger.Error("Error: User with given username or email does not exist.");

        return new AuthResponseVm() {
            Message = "Error: Username, email, or password is incorrect."
        };
    }

    /// <summary>
    /// Implementation of LogoutAsyncRequest.
    /// Logs a user out of the system. 
    /// </summary>
    /// <param name="logoutRequestVm">A request to log a user out. Contains the user's refresh token.</param>
    /// <returns>Boolean denoting success or failure.</returns>
    public Task LogoutAsync(LogoutRequestVm logoutRequestVm) {
        // Get the full token information based on the token passed in by the user.
        return _tokenRepo.GetTokenAndDeleteAsync(logoutRequestVm.RefreshToken);
    }

    /// <summary>
    /// Implementation of ForgotPasswordAsync.
    /// This will get user information and return a tuple containing a reset token and the user information.
    /// </summary>
    /// <param name="forgotPasswordRequestVm">A request to get a password reset. Contains the user's email or username.</param>
    /// <returns>A tuple containing a message string and the User returned from the pull.</returns>

    // Returns a tuple because it can have both a string for the message or the token and the user information in one without the need for a new class.
    public async Task<Tuple<string, AuthUser>> ForgotPasswordAsync(ForgotPasswordRequestVm forgotPasswordRequestVm) {
        AuthUser? user;

        // User sends in either their email or their username.
        if (forgotPasswordRequestVm.UserNameEmail.Contains('@')) {
            user = await _authUserRepo.FindByEmailAsync(forgotPasswordRequestVm.UserNameEmail);
        }
        else {
            user = await _authUserRepo.FindByUsernameAsync(forgotPasswordRequestVm.UserNameEmail);
        }

        return user == null
            ? new Tuple<string, AuthUser>("Error: User not found.", new AuthUser())
            : new Tuple<string, AuthUser>(await _authUserRepo.GeneratePasswordResetTokenAsync(user), user);
    }

    /// <summary>
    /// Implementation of ValidateAndGenerateTokenAsync method.
    /// This method validates that the refresh token passed in is valid and generates a new access token.
    /// </summary>
    /// <param name="jwtTokenHandler">Security token handler for validating JWT.</param>
    /// <param name="tokenRequestVm">A request to validate a token and generate a new token. Contains access token and refresh token.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    public async Task<AuthResponseVm> ValidateAndGenerateTokenAsync(SecurityTokenHandler jwtTokenHandler,
            TokenRequestVm tokenRequestVm) {
        
        RefreshToken storedToken = await _tokenRepo.GetTokenAsync(tokenRequestVm.RefreshToken);

        AuthUser? dbUser = await _authUserRepo.FindByIdAsync(storedToken.UserId.ToString());

        try {
            jwtTokenHandler.ValidateToken(tokenRequestVm.Token, _tokenValidationParameters,
                out _);

            // Happy path. Token is valid.
            if (dbUser != null) {
                return await GenerateJwtAsync(dbUser, storedToken);
            }

            return new AuthResponseVm() {
                Message = "Error: User associated with given token does not exist in system."
            };
        }
        catch (SecurityTokenException) {
            // Unhappy path. Token is expired.
            if (dbUser != null) {
                // Refresh token is unexpired, so regenerate using current refresh token.
                if (storedToken.DateExpired >= DateTime.UtcNow) {
                    return await GenerateJwtAsync(dbUser, storedToken);
                }

                // Refresh token is expired. Generate both refresh and access tokens.
                return await GenerateJwtAsync(dbUser, null);
            }

            return new AuthResponseVm() {
                Message = "Error: User associated with given token does not exist in system."
            };
        }
    }

    /// <summary>
    /// Implementation of RegenerateTokensAsync method.
    /// Provides a way to regenerate tokens without needing to pass in a current token. Used internally only.
    /// </summary>
    /// <param name="user">The user to regenerate a token for.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    public Task<AuthResponseVm> RegenerateTokensAsync(AuthUser user) {
        return GenerateJwtAsync(user, null);
    }

    /// <summary>
    /// Implementation of the GenerateUrl function.
    /// Used to generate a URL for sending in an email.
    /// </summary>
    /// <param name="url">URL of the calling controller.</param>
    /// <param name="action">Action the URL needs to take.</param>
    /// <param name="controller">Controller that will be called in the output URL.</param>
    /// <param name="obj">Query params in the URL.</param>
    /// <param name="protocol">Protocol of the output URL.</param>
    /// <returns>URL in string format.</returns>
    public string? GenerateUrl(IUrlHelper url, string action, string controller, object obj, string protocol) {
        return url.Action(action, controller, obj, protocol);
    }
    #endregion

    #region Private class methods.
    /// <summary>Deletes refresh tokens for a given. There should only ever be 1, but as a fail safe, deletes all.</summary>
    /// <param name="id">ID of User that will have tokens deleted.</param>
    private async Task DeleteRefreshTokensForUser(int id) {
        IEnumerable<RefreshToken> storedTokens = _tokenRepo.FilterByUserIdEqual(id);

        RefreshToken[] refreshTokens = storedTokens as RefreshToken[] ?? storedTokens.ToArray();
        if (refreshTokens.Any()) {
            await _tokenRepo.DeleteMultiple(refreshTokens);
        }
    }

    /// <summary>Helper function that generates the access token for a user.</summary>
    /// <param name="user">User that requested the access token.</param>
    /// <param name="rToken">Optional refresh token. Will be passed to next function and if it's null, regenerated. Note: this is passed in as null explicitly. If passed in as a variable, it will always exist.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    private async Task<AuthResponseVm> GenerateJwtAsync(AuthUser user, RefreshToken? rToken) {
        List<Claim> authClaims;
        if (user is { Email: not null, UserName: not null }) {
            authClaims = new List<Claim> {
                new(ClaimTypes.Name, user.UserName),
                new(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new(JwtRegisteredClaimNames.Email, user.Email),
                new(JwtRegisteredClaimNames.Sub, user.UserName),
                new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            IList<string> userRoles = await _authUserRepo.GetRolesAsync(user);

            authClaims.AddRange(userRoles.Select(role => new Claim(ClaimTypes.Role, role)));
        }
        else {
            return new AuthResponseVm() {
                Message = "Error: Email or Username must be present to generate this token."
            };
        }

        SymmetricSecurityKey authSigningKey = new(
            Encoding.UTF8.GetBytes(_configuration.GetSection("JWT:Secret").Value ?? string.Empty));

        JwtSecurityToken token = new(
            issuer: _configuration["JWT:Issuer"],
            audience: _configuration["JWT:Audience"],
            expires: DateTime.UtcNow.AddMinutes(20),
            claims: authClaims,
            signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha512)
        );

        string? jwtToken = new JwtSecurityTokenHandler().WriteToken(token);

        RefreshToken refreshToken = await GenerateRefreshToken(user, token.Id, rToken);

        return new AuthResponseVm() {
            Message = "Token created successfully!",
            Token = jwtToken,
            RefreshToken = refreshToken.Token,
            ExpiresAt = token.ValidTo
        };
    }

    /// <summary>Generates a refresh token for a user. If refresh token is not null, just return the given token.</summary>
    /// <param name="user">The user that requested the new token.</param>
    /// <param name="tokenId">The id of the access token.</param>
    /// <param name="rToken">Passed in refresh token. If it's null, returns a new token, otherwise returns itself.</param>
    /// <returns>A new refresh token if passed in token is null, otherwise the given refresh token.</returns>
    private async Task<RefreshToken> GenerateRefreshToken(AuthUser user, string tokenId, RefreshToken? rToken) {
        if (rToken != null) {
            return rToken;
        }

        RefreshToken refreshToken = new() {
            JwtId = tokenId,
            IsRevoked = false,
            UserId = user.Id,
            User = user,
            DateAdded = DateTime.UtcNow,
            DateExpired = DateTime.UtcNow.AddMonths(6),
            Token = Convert.ToBase64String(RandomNumberGenerator.GetBytes(512))
        };

        await DeleteRefreshTokensForUser(user.Id);

        await _tokenRepo.InsertAsync(refreshToken);

        return refreshToken;
    }
    #endregion
}