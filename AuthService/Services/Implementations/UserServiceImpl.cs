using AuthService.Data.Models;
using AuthService.Data.Repos;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using AuthService.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NLog;
using NLog.Web;

namespace AuthService.Services.Implementations;

/// <summary>Implementation of UserService business logic.</summary>
public class UserServiceImpl : IUserService {
    
    #region Class members.
    private readonly Logger _logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

    private readonly IAuthUserRepo _authUserRepo;
    private readonly IAuthService _authService;
    #endregion

    #region Ctors.
    /// <summary>Dependency injected constructor for UserServiceImpl class.</summary>
    /// <param name="authUserRepo">Repository to access and update User information.</param>
    /// <param name="authService">Service that houses methods for authenticating users.</param>
    public UserServiceImpl(IAuthUserRepo authUserRepo, IAuthService authService) {
        _authUserRepo = authUserRepo;
        _authService = authService;
    }
    #endregion

    #region Public class methods.
    /// <summary>
    /// Implementation of ConfirmEmail method.
    /// Confirms a user's email and allows them to log in.
    /// </summary>
    /// <param name="emailConfirmationRequestVm">Request containing information needed to confirm a user's email.</param>
    /// <returns>A string with the result of the email confirmation.</returns>
    public async Task<string> ConfirmEmail(EmailConfirmationRequestVm emailConfirmationRequestVm) {
        AuthUser? confirmUser = await _authUserRepo.FindByEmailAsync(emailConfirmationRequestVm.Email);

        if (confirmUser == null) {
            return "Error: User with given username not found.";
        }

        IdentityResult confirmed = await _authUserRepo.ConfirmEmailAsync(confirmUser,
            emailConfirmationRequestVm.ConfirmToken);

        if (confirmed.Succeeded) {
            return "Email confirmed successfully.";
        }

        return "Email confirmation failed. Errors: " + confirmed.Errors;
    }

    /// <summary>
    /// Implementation of the ResendConfirmEmail method.
    /// Regenerates and resends a confirmation token for the given email.
    /// </summary>
    /// <param name="resendConfirmEmailRequestVm">Request containing user's email to send email to.</param>
    /// <returns>Tuple of string with a message and the auth user associated with it.</returns>
    public async Task<Tuple<string, AuthUser>> ResendConfirmEmail(
        ResendConfirmEmailRequestVm resendConfirmEmailRequestVm) {
        AuthUser? user = await _authUserRepo.FindByEmailAsync(resendConfirmEmailRequestVm.Email);

        if (user == null) {
            return new Tuple<string, AuthUser>("Error: User with given email does not exist.", new AuthUser());
        }

        if (user.EmailConfirmed) {
            return new Tuple<string, AuthUser>("Error: Email for user already confirmed.", new AuthUser());
        }

        // Confirm email send.
        _logger.Debug("Resending email to user.");
        string confirmToken = await _authUserRepo.GenerateEmailConfirmationTokenAsync(user);

        return new Tuple<string, AuthUser>(confirmToken, user);
    }

    /// <summary>
    /// Implementation of the ChangePassword method.
    /// Updates a user's password.
    /// </summary>
    /// <param name="passwordUpdateRequestVm">A request with the data needed to update a password.</param>
    /// <param name="userId">The ID of the user requesting to change passwords.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    public async Task<EmailAuthResponseVm> ChangePassword(PasswordUpdateRequestVm passwordUpdateRequestVm,
            string? userId) {
        
        if (userId == null) {
            return new EmailAuthResponseVm() {
                AuthResponse = new AuthResponseVm() {
                    Message = "Error: Calling user cannot be verified as owner of the account changing password."
                }
            };
        }

        AuthUser? user = await _authUserRepo.FindByIdAsync(userId);

        if (user == null) {
            return new EmailAuthResponseVm() {
                AuthResponse = new AuthResponseVm() {
                    Message = "Error: User not found."
                }
            };
        }

        IdentityResult result = await _authUserRepo.ChangePasswordAsync(user, passwordUpdateRequestVm.CurrentPassword,
            passwordUpdateRequestVm.NewPassword);

        // Generate a new token for the user in case their refresh token was compromised. Logs other users out.
        AuthResponseVm tokenReturn = await _authService.RegenerateTokensAsync(user);

        if (result.Succeeded) {
            tokenReturn.Message = "Password successfully updated.";
            return new EmailAuthResponseVm() {
                Email = user.Email,
                Username = user.UserName,
                AuthResponse = tokenReturn
            };
        }

        // Put all the errors together in a single string to be returned.
        string returnString = result.Errors.Aggregate("Error: ", (current, error) => current + (error.Description + ", "));

        if (returnString.Length > 0) {
            returnString = returnString.Substring(0, returnString.Length - 3);
        }

        return new EmailAuthResponseVm() {
            AuthResponse = new AuthResponseVm() {
                Message = "Error: Password could not be updated. Reason(s): " + returnString
            }
        };
    }

    /// <summary>
    /// Implementation of the ChangeUserName method.
    /// Updates a user's username.
    /// </summary>
    /// <param name="usernameUpdateRequestVm">The request with the updated username that the user selected.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    public async Task<EmailAuthResponseVm> ChangeUserName(UsernameUpdateRequestVm usernameUpdateRequestVm) {
        AuthUser? user = await _authUserRepo.FindByIdAsync(usernameUpdateRequestVm.UserId);

        if (user == null) {
            return new EmailAuthResponseVm() {
                AuthResponse = new AuthResponseVm() {
                    Message = "Error: User not found."
                }
            };
        }

        // Update both the username and the normalized username in case it doesn't auto update.
        user.UserName = usernameUpdateRequestVm.UpdatedUsername;
        user.NormalizedUserName = _authUserRepo.NormalizeName(user.UserName);

        IdentityResult result = await _authUserRepo.UpdateAsync(user);

        AuthResponseVm tokenReturn = await _authService.RegenerateTokensAsync(user);

        if (result.Succeeded) {
            tokenReturn.Message = "Username successfully updated.";
            return new EmailAuthResponseVm() {
                Email = user.Email,
                Username = user.UserName,
                AuthResponse = tokenReturn
            };
        }

        // Put all the errors together in a single string.
        string returnString = result.Errors.Aggregate("Error: ", (current, error) => current + (error.Description + ", "));

        if (returnString.Length > 0) {
            returnString = returnString.Substring(0, returnString.Length - 3);
        }

        return new EmailAuthResponseVm() {
            AuthResponse = new AuthResponseVm() {
                Message = "Error: Username could not be updated. Reason(s): " + returnString
            }
        };
    }

    /// <summary>
    /// Implementation of the ResetPassword method.
    /// Resets a user's password if they lost access to their account or forgot their password.
    /// </summary>
    /// <param name="email">The email of the user requesting a password reset.</param>
    /// <param name="newPassword">The new password the user has chosen.</param>
    /// <param name="token">The confirmation token for resetting the password.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    public async Task<AuthResponseVm> ResetPassword(string email, string newPassword, string token) {
        AuthUser? user = await _authUserRepo.FindByEmailAsync(email);

        if (user == null) {
            return new AuthResponseVm() {
                Message = "Error: User not found."
            };
        }

        IdentityResult result = await _authUserRepo.ResetPasswordAsync(user, token, newPassword);

        if (result.Succeeded) {
            return await _authService.RegenerateTokensAsync(user);
        }

        // Put all the errors together in a single string.
        string returnString = result.Errors.Aggregate("Error: ", (current, error) => current + (error.Description + ", "));

        if (returnString.Length > 0) {
            returnString = returnString.Substring(0, returnString.Length - 3);
        }

        return new AuthResponseVm() {
            Message = "Error: Password could not be reset. Reason(s): " + returnString
        };
    }
    
    /// <summary>
    /// Implementation of the GenerateUrl function.
    /// Used to generate a URL for sending in an email.
    /// </summary>
    /// <param name="url">URL of the calling controller.</param>
    /// <param name="action">Action the URL needs to take.</param>
    /// <param name="controller">Controller that will be called in the output URL.</param>
    /// <param name="obj">Query params in the URL.</param>
    /// <param name="protocol">Protocol of the output URL.</param>
    /// <returns>URL in string format.</returns>
    public string? GenerateUrl(IUrlHelper url, string action, string controller, object obj, string protocol) {
        return url.Action(action, controller, obj, protocol);
    }

    /// <summary>
    /// Implementation of the GetUserData method.
    /// Used to return user data for a lucinaravenwing.net application.
    /// </summary>
    /// <param name="userDataRequestVm">Request by another lucinaravenwing.net application to retrieve user data.</param>
    /// <returns>Response containing a message about the return and an email and username for requested user.</returns>
    public async Task<UserDataReturnVm> GetUserData(UserDataRequestVm userDataRequestVm) {
        AuthUser? user = null;

        if (userDataRequestVm.Username != "") {
            user = await _authUserRepo.FindByUsernameAsync(userDataRequestVm.Username);
        } else if (userDataRequestVm.Email != "") {
            user = await _authUserRepo.FindByEmailAsync(userDataRequestVm.Email);
        }

        if (user != null) {
            if (string.IsNullOrEmpty(user.UserName)) {
                return new UserDataReturnVm() {
                    Message = "Error: User found but Username returned null.",
                    Username = "",
                    Email = ""
                };
            }
            
            if (string.IsNullOrEmpty(user.Email)) {
                return new UserDataReturnVm() {
                    Message = "Error: User found but Email returned null.",
                    Username = "",
                    Email = ""
                };
            }
            
            return new UserDataReturnVm() {
                Message = "User found successfully.",
                Username = user.UserName,
                Email = user.Email
            };
        }
        
        return new UserDataReturnVm() {
            Message = "Error: User not found.",
            Username = "",
            Email = ""
        };
    }
    #endregion
}