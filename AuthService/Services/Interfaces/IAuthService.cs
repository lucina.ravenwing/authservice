using AuthService.Data.DTO;
using AuthService.Data.Models;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace AuthService.Services.Interfaces;

/// <summary>Service interface for authentication business logic.</summary>
public interface IAuthService {
    
    /// <summary>
    /// Creates a user if all information is correct and doesn't overlap with existing records.
    /// </summary>
    /// <param name="signUpRequestVm">Request with information needed to sign up a User.</param>
    /// <returns>Data transfer object with return message, email confirmation token, and new user info.</returns>
    Task<SignUpDto> SignUpAsync(SignUpRequestVm signUpRequestVm);

    /// <summary>
    /// Logs a user into the system based on either their username or email and password.
    /// </summary>
    /// <param name="loginVm">Request with information needed to log a user in.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    Task<AuthResponseVm> LoginAsync(LoginRequestVm loginVm);

    /// <summary>
    /// Logs a user out of the system.
    /// </summary>
    /// <param name="logoutRequestVm">A request to log a user out. Contains the user's refresh token.</param>
    /// <returns>Boolean denoting success or failure.</returns>
    Task LogoutAsync(LogoutRequestVm logoutRequestVm);
    
    /// <summary>
    /// This will get user information and return a tuple containing a reset token and the user information.
    /// </summary>
    /// <param name="forgotPasswordRequestVm">A request to get a password reset. Contains the user's email or username.</param>
    /// <returns>A tuple containing a message string and the User returned from the pull.</returns>
    Task<Tuple<string, AuthUser>> ForgotPasswordAsync(ForgotPasswordRequestVm forgotPasswordRequestVm);

    /// <summary>
    /// This method validates that the refresh token passed in is valid and generates a new access token.
    /// </summary>
    /// <param name="jwtTokenHandler">Security token handler for validating JWT.</param>
    /// <param name="tokenRequestVm">A request to validate a token and generate a new token. Contains access token and refresh token.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    Task<AuthResponseVm> ValidateAndGenerateTokenAsync(SecurityTokenHandler jwtTokenHandler, TokenRequestVm tokenRequestVm);
    
    /// <summary>
    /// Provides a way to regenerate tokens without needing to pass in a current token. Used internally only.
    /// </summary>
    /// <param name="user">The user to regenerate a token for.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    Task<AuthResponseVm> RegenerateTokensAsync(AuthUser user);

    /// <summary>
    /// Used to generate a URL for sending in an email.
    /// </summary>
    /// <param name="url">URL of the calling controller.</param>
    /// <param name="action">Action the URL needs to take.</param>
    /// <param name="controller">Controller that will be called in the output URL.</param>
    /// <param name="obj">Query params in the URL.</param>
    /// <param name="protocol">Protocol of the output URL.</param>
    /// <returns>URL in string format.</returns>
    string? GenerateUrl(IUrlHelper url, string action, string controller, object obj, string protocol);
}