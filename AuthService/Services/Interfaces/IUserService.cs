using AuthService.Data.Models;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using Microsoft.AspNetCore.Mvc;

namespace AuthService.Services.Interfaces; 
/// <summary>Service interface for user business logic.</summary>
public interface IUserService {

    /// <summary>
    /// Confirms a user's email and allows them to log in.
    /// </summary>
    /// <param name="emailConfirmationRequestVm">Request containing information needed to confirm a user's email.</param>
    /// <returns>A string with the result of the email confirmation.</returns>
    Task<string> ConfirmEmail(EmailConfirmationRequestVm emailConfirmationRequestVm);

    /// <summary>
    /// Regenerates and resends a confirmation token for the given email.
    /// </summary>
    /// <param name="resendConfirmEmailRequestVm">Request containing user's email to send email to.</param>
    /// <returns>Tuple of string with a message and the auth user associated with it.</returns>
    Task<Tuple<string, AuthUser>> ResendConfirmEmail(ResendConfirmEmailRequestVm resendConfirmEmailRequestVm);

    /// <summary>
    /// Updates a user's password.
    /// </summary>
    /// <param name="passwordUpdateRequestVm">A request with the data needed to update a password.</param>
    /// <param name="userId">The ID of the user requesting to change passwords.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    Task<EmailAuthResponseVm> ChangePassword(PasswordUpdateRequestVm passwordUpdateRequestVm, string? userId);

    /// <summary>
    /// Updates a user's username.
    /// </summary>
    /// <param name="usernameUpdateRequestVm">The request with the updated username that the user selected.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    Task<EmailAuthResponseVm> ChangeUserName(UsernameUpdateRequestVm usernameUpdateRequestVm);

    /// <summary>
    /// Resets a user's password if they lost access to their account or forgot their password.
    /// </summary>
    /// <param name="email">The email of the user requesting a password reset.</param>
    /// <param name="newPassword">The new password the user has chosen.</param>
    /// <param name="token">The confirmation token for resetting the password.</param>
    /// <returns>Response containing a message about the return, an access token, a refresh token, and information on expiration.</returns>
    Task<AuthResponseVm> ResetPassword(string email, string newPassword, string token);
    
    /// <summary>
    /// Used to generate a URL for sending in an email.
    /// </summary>
    /// <param name="url">URL of the calling controller.</param>
    /// <param name="action">Action the URL needs to take.</param>
    /// <param name="controller">Controller that will be called in the output URL.</param>
    /// <param name="obj">Query params in the URL.</param>
    /// <param name="protocol">Protocol of the output URL.</param>
    /// <returns>URL in string format.</returns>
    string? GenerateUrl(IUrlHelper url, string action, string controller, object obj, string protocol);

    /// <summary>
    /// Used to return user data for a lucinaravenwing.net application.
    /// </summary>
    /// <param name="userDataRequestVm">Request by another lucinaravenwing.net application to retrieve user data.</param>
    /// <returns>Response containing a message about the return and an email and username for requested user.</returns>
    Task<UserDataReturnVm> GetUserData(UserDataRequestVm userDataRequestVm);

}