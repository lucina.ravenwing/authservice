using AuthService.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace AuthService.Data.Repos; 

/// <summary>
/// Interface with methods that will expose EntityFramework to the rest of the application.
/// </summary>
public interface IAuthUserRepo {
    /// <summary>
    /// Creates a user using EF.
    /// </summary>
    /// <param name="user">User to be created.</param>
    /// <param name="password">Password of the user being created.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    Task<IdentityResult> CreateAsync(AuthUser user, string password);

    /// <summary>
    /// Adds a user to a role. Or vice versa. However you want to look at it.
    /// </summary>
    /// <param name="user">User to add role to.</param>
    /// <param name="role">Role to add.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    Task<IdentityResult> AddToRoleAsync(AuthUser user, string role);
    
    /// <summary>
    /// Searches for a user with given email.
    /// </summary>
    /// <param name="email">Email to search.</param>
    /// <returns>User with given email.</returns>
    Task<AuthUser?> FindByEmailAsync(string email);
    
    /// <summary>
    /// Searches for a user with given username.
    /// </summary>
    /// <param name="username">Username to search.</param>
    /// <returns>User with given username.</returns>
    Task<AuthUser?> FindByUsernameAsync(string username);

    /// <summary>
    /// Searches for a user with given ID.
    /// </summary>
    /// <param name="id">Id to search.</param>
    /// <returns>User with given Id.</returns>
    Task<AuthUser?> FindByIdAsync(string id);

    /// <summary>
    /// Returns list of roles for given user.
    /// </summary>
    /// <param name="user">User to find roles associated with.</param>
    /// <returns>List of roles that the user has.</returns>
    Task<IList<string>> GetRolesAsync(AuthUser user);

    /// <summary>
    /// Returns a username in all caps.
    /// </summary>
    /// <param name="name">Name to be normalized.</param>
    /// <returns>Normalized (caps) name.</returns>
    string NormalizeName(string name);

    /// <summary>
    /// Checks that a given password matches the stored encrypted password.
    /// </summary>
    /// <param name="user">User to check the password against.</param>
    /// <param name="password">Password to check.</param>
    /// <returns>Boolean on whether password matches or not.</returns>
    Task<bool> CheckPasswordAsync(AuthUser user, string password);

    /// <summary>
    /// Changes a user's password.
    /// </summary>
    /// <param name="user">User to change the password.</param>
    /// <param name="currentPassword">User's current password.</param>
    /// <param name="newPassword">User's new password.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    Task<IdentityResult> ChangePasswordAsync(AuthUser user, string currentPassword, string newPassword);

    /// <summary>
    /// Generates a token that verifies a user's attempt to reset a password.
    /// </summary>
    /// <param name="user">User to generate a reset token for.</param>
    /// <returns>The generated reset token.</returns>
    Task<string> GeneratePasswordResetTokenAsync(AuthUser user);

    /// <summary>
    /// Reset's a user's password if the confirmation token is verified.
    /// </summary>
    /// <param name="user">User whose password is getting reset.</param>
    /// <param name="confirmToken">Confirmation token to ensure it is the proper user.</param>
    /// <param name="newPassword">What the password will be reset to.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    Task<IdentityResult> ResetPasswordAsync(AuthUser user, string confirmToken, string newPassword);

    /// <summary>
    /// Checks if the user has verified their email.
    /// </summary>
    /// <param name="user">User to check status of email confirmation.</param>
    /// <returns>Boolean on if the email is confirmed or not.</returns>
    Task<bool> IsEmailConfirmedAsync(AuthUser user);
    
    /// <summary>
    /// Generates a token that is used to verify a user's attempt to confirm their password.
    /// </summary>
    /// <param name="user">User to generate confirmation token for.</param>
    /// <returns>Generated confirmation token.</returns>
    Task<string> GenerateEmailConfirmationTokenAsync(AuthUser user);

    /// <summary>
    /// Confirms a user's email if the confirmation token is verified.
    /// </summary>
    /// <param name="user">User whose email is being confirmed.</param>
    /// <param name="confirmToken">Confirmation token to ensure it is the correct user.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    Task<IdentityResult> ConfirmEmailAsync(AuthUser user, string confirmToken);

    /// <summary>
    /// Updates a user in the database.
    /// </summary>
    /// <param name="user">User to be updated.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    Task<IdentityResult> UpdateAsync(AuthUser user);

    /// <summary>
    /// Increments a user's failed attempts count.
    /// </summary>
    /// <param name="user">User to increment access failed count.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    Task<IdentityResult> AccessFailedAsync(AuthUser user);

    /// <summary>
    /// Resets a user's failed attempts count.
    /// </summary>
    /// <param name="user">User whose access failed count will be reset.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    Task<IdentityResult> ResetAccessFailedCountAsync(AuthUser user);
}