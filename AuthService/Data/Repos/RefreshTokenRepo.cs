using AuthService.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace AuthService.Data.Repos; 

/// <summary>
/// Implementation of Refresh Token Repo interface. Used to control what calls can be made into the database.
/// </summary>
public class RefreshTokenRepo : IRefreshTokenRepo {

    #region Class members.
    private readonly AuthDbContext _context;
    #endregion

    #region Ctors
    /// <summary>
    /// All args constructor for RefreshTokenRepo
    /// </summary>
    /// <param name="context">The database context for the project.</param>
    public RefreshTokenRepo(AuthDbContext context) {
        _context = context;
    }
    #endregion
    
    #region Public class methods.
    /// <summary>
    /// Get a given refresh token from the database. Returns a blank refresh token if none are returned.
    /// </summary>
    /// <param name="token">Token to search.</param>
    /// <returns>The full token object with given token.</returns>
    public async Task<RefreshToken> GetTokenAsync(string token) {
        // Attempt to get the refresh token. If it's null, set it to a blank refresh token.
        RefreshToken returnedToken = await _context.RefreshToken
                                         .FirstOrDefaultAsync(tokenQuery => tokenQuery.Token == token)
            ?? new RefreshToken();
        
        return returnedToken;
    }

    /// <summary>
    /// Retrieve the token given, get the associated user, and delete all tokens associated with the user.
    /// This is used for logging out. Both the get and delete functions are in this one function as the database gets
    /// disposed if they are separate.
    /// </summary>
    /// <param name="token">Token to search.</param>
    /// <returns>The full token object with given token.</returns>
    public async Task GetTokenAndDeleteAsync(string token) {
        RefreshToken? returnedToken = _context.RefreshToken.FirstOrDefault(tokenQuery => tokenQuery.Token == token);

        // Get the refresh tokens from the database with the user id associated with the original user id, then delete
        // all of them.
        if (returnedToken != null) {
            IQueryable<RefreshToken> tokens = _context.RefreshToken.Where(tokenQuery => tokenQuery.UserId == returnedToken.UserId);
            
            await DeleteMultiple(tokens);
        }
    }

    /// <summary>
    /// Get the tokens associated with the given user id.
    /// </summary>
    /// <param name="userId">The ID to filter the refresh tokens on.</param>
    /// <returns>An enumerable of Refresh Tokens.</returns>
    public IEnumerable<RefreshToken> FilterByUserIdEqual(int userId) {
        return _context.RefreshToken.Where(tokenQuery => tokenQuery.UserId == userId);
    }

    /// <summary>
    /// Delete the tokens in the passed in Enumerable object.
    /// </summary>
    /// <param name="refreshTokens">List of refresh tokens to delete.</param>
    /// <returns>Entity State informing success or failure.</returns>
    public async Task<EntityState> DeleteMultiple(IEnumerable<RefreshToken> refreshTokens) {
        _context.RemoveRange(refreshTokens);
        
        // If the number of changes saved to the database is greater than 0, then return deleted, otherwise unchanged.
        return await _context.SaveChangesAsync() > 0 ? EntityState.Deleted : EntityState.Unchanged;
    }

    /// <summary>
    /// Insert a refresh token into the database.
    /// </summary>
    /// <param name="refreshToken">Refresh Token to insert into database.</param>
    /// <returns>Entity State informing success or failure.</returns>
    public async Task<EntityState> InsertAsync(RefreshToken refreshToken) { 
        EntityEntry<RefreshToken> addResult = await _context.AddAsync(refreshToken);

        // If the state for the add command is added, then save and return that result. Otherwise return unchanged.
        if (addResult.State == EntityState.Added) {
            return await _context.SaveChangesAsync() > 0 ? EntityState.Added : EntityState.Unchanged;
        }

        return EntityState.Unchanged;
    }
    #endregion
}