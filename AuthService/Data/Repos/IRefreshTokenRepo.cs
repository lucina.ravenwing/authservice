using AuthService.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace AuthService.Data.Repos; 

/// <summary>
/// Interface for Refresh Tokens. Hides the full functionality of Entity Framework and allows for easier testing.
/// </summary>
public interface IRefreshTokenRepo {
    
    /// <summary>
    /// Get a given refresh token from the database.
    /// </summary>
    /// <param name="token">Token to search.</param>
    /// <returns>The full token object with given token.</returns>
    Task<RefreshToken> GetTokenAsync(string token);

    /// <summary>
    /// Retrieve the token given, get the associated user, and delete all tokens associated with the user.
    /// This is used for logging out.
    /// </summary>
    /// <param name="token">Token to search.</param>
    /// <returns>The full token object with given token.</returns>
    Task GetTokenAndDeleteAsync(string token);

    /// <summary>
    /// Get the tokens associated with the given user id.
    /// </summary>
    /// <param name="userId">The ID to filter the refresh tokens on.</param>
    /// <returns>An enumerable of Refresh Tokens.</returns>
    IEnumerable<RefreshToken> FilterByUserIdEqual(int userId);

    /// <summary>
    /// Delete the tokens in the passed in Enumerable object.
    /// </summary>
    /// <param name="refreshTokens">List of refresh tokens to delete.</param>
    /// <returns>Entity State informing success or failure.</returns>
    Task<EntityState> DeleteMultiple(IEnumerable<RefreshToken> refreshTokens);

    /// <summary>
    /// Insert a refresh token into the database.
    /// </summary>
    /// <param name="refreshToken">Refresh Token to insert into database.</param>
    /// <returns>Entity State informing success or failure.</returns>
    Task<EntityState> InsertAsync(RefreshToken refreshToken);
}