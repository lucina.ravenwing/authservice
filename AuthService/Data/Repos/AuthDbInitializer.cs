using AuthService.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace AuthService.Data.Repos; 

/// <summary>Initializer class for database. Initializes the roles in the database.</summary>
public static class AuthDbInitializer {
    
    /// <summary>Method to set the roles in the database on startup.</summary>
    /// <param name="applicationBuilder">Application builder of the project to add the RoleManager provider.</param>
    public static async Task SeedRolesToDatabase(IApplicationBuilder applicationBuilder) {
        using IServiceScope serviceScope = applicationBuilder.ApplicationServices.CreateScope();
        
        RoleManager<IdentityRole<int>> roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole<int>>>();

        if (!await roleManager.RoleExistsAsync(UserRoles.User)) {
            await roleManager.CreateAsync(new IdentityRole<int>(UserRoles.User));
        }
        if (!await roleManager.RoleExistsAsync(UserRoles.Moderator)) {
            await roleManager.CreateAsync(new IdentityRole<int>(UserRoles.Moderator));
        }
        if (!await roleManager.RoleExistsAsync(UserRoles.Admin)) {
            await roleManager.CreateAsync(new IdentityRole<int>(UserRoles.Admin));
        }
    }
}