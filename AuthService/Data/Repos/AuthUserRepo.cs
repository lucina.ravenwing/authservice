using AuthService.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace AuthService.Data.Repos; 

/// <summary>
/// Implementation of the IAuthUserRepo. This is used to expose only the necessary functionality for EntityFramework
/// as well as allow for an easier time testing.
/// </summary>
public class AuthUserRepo : IAuthUserRepo {
    
    #region Class members.
    private readonly UserManager<AuthUser> _userManager;
    #endregion

    #region Ctors
    /// <summary>
    /// All args constructor for RefreshTokenRepo
    /// </summary>
    /// <param name="userManager">The manager used in Identity Framework.</param>
    public AuthUserRepo(UserManager<AuthUser> userManager) {
        _userManager = userManager;
    }
    #endregion

    #region Public class methods.
    /// <summary>
    /// Implementation of the CreateAsync method. Creates a user using EF.
    /// </summary>
    /// <param name="user">User to be created.</param>
    /// <param name="password">Password of the user being created.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    public Task<IdentityResult> CreateAsync(AuthUser user, string password) {
        return _userManager.CreateAsync(user, password);
    }

    /// <summary>
    /// Implementation of the AddToRoleAsync method. Adds a user to a role. Or vice versa. However you want to look at it.
    /// </summary>
    /// <param name="user">User to add role to.</param>
    /// <param name="role">Role to add.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    public Task<IdentityResult> AddToRoleAsync(AuthUser user, string role) {
        return _userManager.AddToRoleAsync(user, role);
    }

    /// <summary>
    /// Implementation of the FindByEmailAsync method. Searches for a user with given email.
    /// </summary>
    /// <param name="email">Email to search.</param>
    /// <returns>User with given email.</returns>
    public Task<AuthUser?> FindByEmailAsync(string email) {
        return _userManager.FindByEmailAsync(email);
    }
    
    /// <summary>
    /// Implementation of the FindByUsernameAsync method. Searches for a user with given username.
    /// </summary>
    /// <param name="username">Username to search.</param>
    /// <returns>User with given username.</returns>
    public Task<AuthUser?> FindByUsernameAsync(string username) { 
        return _userManager.FindByNameAsync(username);
    }

    /// <summary>
    /// Implementation of the FindById method. Searches for a user with given ID.
    /// </summary>
    /// <param name="id">Id to search.</param>
    /// <returns>User with given Id.</returns>
    public Task<AuthUser?> FindByIdAsync(string id) {
        return _userManager.FindByIdAsync(id);
    }

    /// <summary>
    /// Implementation of the GetRolesAsync class. Returns list of roles for given user.
    /// </summary>
    /// <param name="user">User to find roles associated with.</param>
    /// <returns>List of roles that the user has.</returns>
    public Task<IList<string>> GetRolesAsync(AuthUser user) {
        return _userManager.GetRolesAsync(user);
    }
    
    /// <summary>
    /// Implementation of the NormalizeName method. Returns a username in all caps.
    /// </summary>
    /// <param name="name">Name to be normalized.</param>
    /// <returns>Normalized (caps) name.</returns>
    public string NormalizeName(string name) {
        return _userManager.NormalizeName(name);
    }

    /// <summary>
    /// Implementation of the CheckPasswordAsync method. Checks that a given password matches the stored encrypted password.
    /// </summary>
    /// <param name="user">User to check the password against.</param>
    /// <param name="password">Password to check.</param>
    /// <returns>Boolean on whether password matches or not.</returns>
    public Task<bool> CheckPasswordAsync(AuthUser user, string password) {
        return _userManager.CheckPasswordAsync(user, password);
    }

    /// <summary>
    /// Implementation of the ChangePasswordAsync method. Changes a user's password.
    /// </summary>
    /// <param name="user">User to change the password.</param>
    /// <param name="currentPassword">User's current password.</param>
    /// <param name="newPassword">User's new password.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    public Task<IdentityResult> ChangePasswordAsync(AuthUser user, string currentPassword, string newPassword) {
        return _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
    }

    /// <summary>
    /// Implementation of the GeneratePasswordResetTokenAsync method. Generates a token that verifies a user's
    /// attempt to reset a password.
    /// </summary>
    /// <param name="user">User to generate a reset token for.</param>
    /// <returns>The generated reset token.</returns>
    public Task<string> GeneratePasswordResetTokenAsync(AuthUser user) {
        return _userManager.GeneratePasswordResetTokenAsync(user);
    }
    
    /// <summary>
    /// Implementation of the ResetPasswordAsync method. Reset's a user's password if the confirmation token is verified.
    /// </summary>
    /// <param name="user">User whose password is getting reset.</param>
    /// <param name="confirmToken">Confirmation token to ensure it is the proper user.</param>
    /// <param name="newPassword">What the password will be reset to.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    public Task<IdentityResult> ResetPasswordAsync(AuthUser user, string confirmToken, string newPassword) {
        return _userManager.ResetPasswordAsync(user, confirmToken, newPassword);
    }

    /// <summary>
    /// Implementation of the IsEmailConfirmedAsync method. Checks if the user has verified their email.
    /// </summary>
    /// <param name="user">User to check status of email confirmation.</param>
    /// <returns>Boolean on if the email is confirmed or not.</returns>
    public Task<bool> IsEmailConfirmedAsync(AuthUser user) {
        return _userManager.IsEmailConfirmedAsync(user);
    }
    
    /// <summary>
    /// Implementation of the GenerateEmailConfirmationTokenAsync method. Generates a token that is used to verify a
    /// user's attempt to confirm their password.
    /// </summary>
    /// <param name="user">User to generate confirmation token for.</param>
    /// <returns>Generated confirmation token.</returns>
    public Task<string> GenerateEmailConfirmationTokenAsync(AuthUser user) {
        return _userManager.GenerateEmailConfirmationTokenAsync(user);
    }

    /// <summary>
    /// Implementation of the ConfirmEmailAsync method. Confirms a user's email if the confirmation token is verified.
    /// </summary>
    /// <param name="user">User whose email is being confirmed.</param>
    /// <param name="confirmToken">Confirmation token to ensure it is the correct user.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    public Task<IdentityResult> ConfirmEmailAsync(AuthUser user, string confirmToken) {
        return _userManager.ConfirmEmailAsync(user, confirmToken);
    }

    /// <summary>
    /// Implementation of the UpdateAsync method. Updates a user in the database.
    /// </summary>
    /// <param name="user">User to be updated.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    public Task<IdentityResult> UpdateAsync(AuthUser user) {
        return _userManager.UpdateAsync(user);
    }

    /// <summary>
    /// Implementation of the AccessFailedAsync method. Increments a user's failed attempts count.
    /// </summary>
    /// <param name="user">User to increment access failed count.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    public Task<IdentityResult> AccessFailedAsync(AuthUser user) {
        return _userManager.AccessFailedAsync(user);
    }

    /// <summary>
    /// Implementation of the ResetAccessFailedCountAsync method. Resets a user's failed attempts count.
    /// </summary>
    /// <param name="user">User whose access failed count will be reset.</param>
    /// <returns>An IdentityResult with information whether the transaction completed or not.</returns>
    public Task<IdentityResult> ResetAccessFailedCountAsync(AuthUser user) {
        return _userManager.ResetAccessFailedCountAsync(user);
    }
    #endregion
}