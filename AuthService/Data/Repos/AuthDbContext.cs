using AuthService.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AuthService.Data.Repos; 

/// <summary>Database context for AuthService. Extends Identity context to inherit user information.</summary>
public class AuthDbContext : IdentityDbContext<AuthUser, IdentityRole<int>, int> {
    /// <summary>
    /// Default constructor for AuthDbContext.
    /// </summary>
    public AuthDbContext() { }

    /// <summary>Dependency injected constructor that calls the base class constructor.</summary>
    /// <param name="options"></param>
    public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options) { }

    /// <summary>Adds RefreshToken as an entity and table to the database context.</summary>
    public virtual DbSet<RefreshToken> RefreshToken { get; set; } = null!;
}