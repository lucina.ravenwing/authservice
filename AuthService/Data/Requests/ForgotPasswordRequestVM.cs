using System.ComponentModel.DataAnnotations;

namespace AuthService.Data.Requests; 

/// <summary>Request for a password reset token.</summary>
public class ForgotPasswordRequestVm {

    /// <summary>The username or email of the user asking for a reset.</summary>
    /// <example>test@test.com</example>
    [Required]
    public string UserNameEmail { get; set; } = "";

    /// <summary>
    /// The origin of the request to route back after completing password reset.
    /// </summary>
    /// <example>https://ygo.lucinaravenwing.net</example>
    public string Origin { get; set; } = "";

}