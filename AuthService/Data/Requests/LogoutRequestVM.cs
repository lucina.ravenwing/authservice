using System.ComponentModel.DataAnnotations;

namespace AuthService.Data.Requests; 

/// <summary>Request to log a user out of the application.</summary>
public class LogoutRequestVm {
    /// <summary>User's refresh token.</summary>
    /// <example>dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0=</example>
    [Required] 
    public string RefreshToken { get; set; } = "";
}