using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AuthService.Data.Requests; 

/// <summary>
/// Request object to get user data.
/// </summary>
[ApiExplorerSettings(IgnoreApi = true)]
public class UserDataRequestVm {
    /// <summary>
    /// The key to validate the user has access to get this data.
    /// </summary>
    [Required] public string Key { get; set; } = "";
    
    /// <summary>
    /// The username of the user whose data is being retrieved.
    /// </summary>
    /// <example>auraravenwing</example>
    public string Username { get; set; } = "";

    /// <summary>
    /// The email of the user whose data is being retrieved.
    /// </summary>
    /// <example>lucinaravenwing.net@gmail.com</example>
    public string Email { get; set; } = "";
}