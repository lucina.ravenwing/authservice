using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AuthService.Data.Requests; 

/// <summary>Request to confirm a user's email account.</summary>
public class EmailConfirmationRequestVm {

    /// <summary>User's email.</summary>
    /// <example>test@test.com</example>
    [Required] 
    [FromQuery(Name = "email")] 
    public string Email { get; set; } = "";

    /// <summary>Token generated to confirm a user's email.</summary>
    /// <example>CfDJ8BrK8kI16thIvEKAXiaCocP7VZcTBf7QapAR2j+EEY47ocqULpsJSgY8wnd7xgBfpVrsHwZ83T968rEnTXxF2DRZfOdPSZhN5sZ861HXTRPf0k2cihxytse+Tg7Y6A8M12HqpPAMpBxpVvinCLtM9O8mZs+b0xPFEms6J/OgMUTuUMLDlMEJmY/NRCo6AKJWDg==</example>
    [Required]
    [FromQuery(Name = "ConfirmToken")]
    public string ConfirmToken { get; set; } = "";

    /// <summary>
    /// Origin of the request for confirmation.
    /// </summary>
    /// <example>ygo.lucinaravenwing.net</example>
    [FromQuery(Name = "origin")] 
    public string Origin { get; set; } = "";

}