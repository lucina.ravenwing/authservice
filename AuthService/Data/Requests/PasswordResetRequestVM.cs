using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace AuthService.Data.Requests; 

/// <summary>Request to reset a user's password.</summary>
public class PasswordResetRequestVm {
    /// <summary>Reset token generated by application. Used to verify request.</summary>
    /// <example>"CfDJ8BrK8kI16thIvEKAXiaCocP7VZcTBf7QapAR2j%2BEEY47ocqULpsJSgY8wnd7xgBfpVrsHwZ83T968rEnTXxF2DRZfOdPSZhN5sZ861HXTRPf0k2cihxytse%2BTg7Y6A8M12HqpPAMpBxpVvinCLtM9O8mZs%2Bb0xPFEms6J%2FOgMUTuUMLDlMEJmY%2FNRCo6AKJWDg%3D%3D</example>
    [Required]
    [SuppressMessage("ReSharper", "CommentTypo")]
    public string Token { get; set; } = "";
    /// <summary>Password that will be set after the reset.</summary>
    /// <example>Password_2</example>
    [Required]
    public string NewPassword { get; set; } = "";
    /// <summary>Email of the user to reset.</summary>
    /// <example>test@test.com</example>
    [Required]
    public string Email { get; set; } = "";

    /// <summary>
    /// The origin of the request to route back after completing password reset.
    /// </summary>
    /// <example>https://ygo.lucinaravenwing.net</example>
    public string Origin { get; set; } = "";
}