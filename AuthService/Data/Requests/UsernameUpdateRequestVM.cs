using System.ComponentModel.DataAnnotations;

namespace AuthService.Data.Requests; 

/// <summary>Request used to update a user's username.</summary>
public class UsernameUpdateRequestVm {
    /// <summary>
    /// Id of the user requesting the username update.
    /// </summary>
    /// <example>1</example>
    [Required] public string UserId { get; set; } = "0";
    /// <summary>The username to update to.</summary>
    /// <example>testUser2</example>
    [Required] public string UpdatedUsername { get; set; } = "";
}