using System.ComponentModel.DataAnnotations;

namespace AuthService.Data.Requests; 

/// <summary>Request to resend a confirmation email to a user.</summary>
public class ResendConfirmEmailRequestVm {
    
    /// <summary>The email of the user asking to resend confirmation email.</summary>
    /// <example>test@test.com</example>
    [Required]
    public string Email { get; set; } = "";

    /// <summary>
    /// Origin of the request to be redirected back to.
    /// </summary>
    /// <example>ygo.lucinaravenwing.net</example>
    public string RequestOrigin { get; set; } = "";
}