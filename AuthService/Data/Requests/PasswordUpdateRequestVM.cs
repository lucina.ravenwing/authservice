using System.ComponentModel.DataAnnotations;

namespace AuthService.Data.Requests; 

/// <summary>Request to update a user's password.</summary>
public class PasswordUpdateRequestVm {
    /// <summary>
    /// User id of the given user.
    /// </summary>
    /// <example>1</example>
    public string UserId { get; set; } = "0";
    
    /// <summary>User's current password.</summary>
    /// <example>Password_1</example>
    [Required] 
    public string CurrentPassword { get; set; } = "";
    /// <summary>Password user would like to change to.</summary>
    /// <example>Password_2</example>
    [Required] 
    public string NewPassword { get; set; } = "";

    /// <summary>
    /// Confirmation for new password to ensure user entered correctly.
    /// </summary>
    /// <example>Password_2</example>
    [Required]
    public string ConfirmNewPassword { get; set; } = "";
}