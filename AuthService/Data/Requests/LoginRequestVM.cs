using System.ComponentModel.DataAnnotations;

namespace AuthService.Data.Requests; 

/// <summary>Request used to log a user in.</summary>
public class LoginRequestVm {

    /// <summary>Username or email of an account to log into.</summary>
    /// <example>test@gmail.com</example>
    [Required]
    public string UserNameEmail { get; set; } = "";
    /// <summary>Password of the account to log into.</summary>
    /// <example>Password_1</example>
    [Required] 
    public string Password { get; set; } = "";

}