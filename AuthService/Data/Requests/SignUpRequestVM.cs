using System.ComponentModel.DataAnnotations;

namespace AuthService.Data.Requests;

/// <summary>Request used to sign up a user.</summary>
public class SignUpRequestVm {
    /// <summary>User's email address.</summary>
    /// <example>test@test.com</example>
    [Required]
    public string EmailAddress { get; set; } = "";
    /// <summary>Username chosen by the user. Must be 6 characters long.</summary>
    /// <example>testUser</example>
    [Required]
    [MinLength(6)]
    public string Username { get; set; } = "";
    /// <summary>User's password. Must contain 1 uppercase, 1 lowercase, 1 number, and 1 special character.</summary>
    /// <example>Password_1</example>
    [Required] 
    [MinLength(6)]
    public string Password { get; set; } = "";
    /// <summary>Confirmation of user's password. Must match password.</summary>
    /// <example>Password_1</example>
    [Required] 
    [MinLength(6)]
    public string ConfirmPassword { get; set; } = "";

    /// <summary>
    /// Used to redirect a user back to the proper site after the user has successfully confirmed their email.
    /// </summary>
    /// <example>https://ygo.lucinaravenwing.net</example>
    public string RequestOrigin { get; set; } = "";
}