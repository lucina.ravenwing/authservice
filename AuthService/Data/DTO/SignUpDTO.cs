using AuthService.Data.Models;

namespace AuthService.Data.DTO; 

/// <summary>Data transfer object to transfer information between classes.</summary>
public class SignUpDto {
    /// <summary>Message to return to the user.</summary>
    public string Message { get; set; } = "";
    /// <summary>Confirm token to be sent in an email.</summary>
    public string ConfirmToken { get; set; } = "";
    /// <summary>User to send an email to.</summary>
    public AuthUser NewUser { get; set; } = new();
}