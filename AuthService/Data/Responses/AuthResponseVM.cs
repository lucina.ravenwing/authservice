using System.Diagnostics.CodeAnalysis;

namespace AuthService.Data.Responses; 

/// <summary>Response for an Auth attempt.</summary>
public class AuthResponseVm {
    /// <summary>Message returned to the user.</summary>
    /// <example>Token created successfully!</example>
    public string Message { get; set; } = "";
    /// <summary>Access token for given user.</summary>
    /// <example>eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg</example>
    [SuppressMessage("ReSharper", "CommentTypo")]
    public string Token { get; set; } = "";
    /// <summary>Refresh token for given user.</summary>
    /// <example>dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0=</example>
    public string RefreshToken { get; set; } = "";
    /// <summary>Time that the token expires (UTC).</summary>
    /// <example>2023-01-17T05:14:42Z</example>
    public DateTime ExpiresAt { get; set; } = DateTime.UtcNow;
}