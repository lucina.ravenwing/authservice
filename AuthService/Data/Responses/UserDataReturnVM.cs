using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace AuthService.Data.Responses; 

/// <summary>
/// Return object for requesting user data if a user hasn't logged into an application previously.
/// </summary>
[ApiExplorerSettings(IgnoreApi = true)]
public class UserDataReturnVm {
    /// <summary>
    /// The message to return to a user.
    /// </summary>
    [Required]
    public string Message { get; set; } = "";
    
    /// <summary>
    /// The username of the user being requested.
    /// </summary>
    public string Username { get; set; } = "";
    
    /// <summary>
    /// The email of the user being requested.
    /// </summary>
    public string Email { get; set; } = "";
}