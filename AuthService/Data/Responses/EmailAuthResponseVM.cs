namespace AuthService.Data.Responses; 

/// <summary>
/// Adds username and email as returns for the AuthResponseVM.
/// </summary>
public class EmailAuthResponseVm {
    /// <summary>
    /// Username of the user returned by the calling method.
    /// </summary>
    public string? Username { get; set; } = "";
    /// <summary>
    /// Email of the user returned by the calling method.
    /// </summary>
    public string? Email { get; set; } = "";
    /// <summary>
    /// AuthResponse returned from the calling method.
    /// </summary>
    public AuthResponseVm? AuthResponse { get; set; }
}