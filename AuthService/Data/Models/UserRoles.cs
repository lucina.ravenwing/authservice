namespace AuthService.Data.Models; 

/// <summary>Enumeration like set up for the roles </summary>
public static class UserRoles {
    /// <summary>Denotes user role. Base access.</summary>
    public const string User = "user";
    /// <summary>Denotes moderator role. Enhanced access.</summary>
    public const string Moderator = "moderator";
    /// <summary>Denotes admin role. All access.</summary>
    public const string Admin = "admin";
}