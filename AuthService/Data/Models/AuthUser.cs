using Microsoft.AspNetCore.Identity;

namespace AuthService.Data.Models; 

/// <summary>User class that is used throughout the app. Inherits from the Identity User and changes the ID to an integer type.</summary>
public class AuthUser : IdentityUser<int> {
    
}