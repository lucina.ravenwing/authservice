using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AuthService.Data.Models; 

/// <summary>Refresh token model class to pull from database table.</summary>
public class RefreshToken {
    /// <summary>Id of the table.</summary>
    [Key] public int Id { get; set; }

    /// <summary>Literal value of the token stored in the database.</summary>
    public string Token { get; set; } = "";

    /// <summary>ID of the initial access token associated with the refresh token.</summary>
    public string JwtId { get; set; } = "";

    /// <summary>Boolean to determine if the token has been revoked.</summary>
    public bool IsRevoked { get; set; }

    /// <summary>Date that the token was added to the database.</summary>
    public DateTime DateAdded { get; set; } = DateTime.UtcNow;
    /// <summary>Date that the token expires.</summary>
    public DateTime DateExpired { get; set; } = DateTime.UtcNow.AddMinutes(20);

    /// <summary>ID of the user associated with the token.</summary>
    public int UserId { get; set; }
    /// <summary>User object associated with the user ID.</summary>
    [ForeignKey(nameof(UserId))] public AuthUser User { get; set; } = new();
}