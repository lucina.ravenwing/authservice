using System.Reflection;
using System.Text;
using AuthService.Data.Models;
using AuthService.Data.Repos;
using AuthService.ErrorHandling;
using AuthService.Services.Implementations;
using AuthService.Services.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog;
using NLog.Web;

Logger logger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();
logger.Debug("App starting.");

const string allowedOrigins = "AllowLucinaOrigins";

try {
    WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

// Enable CORS for lucinaravenwing.net sites.
    builder.Services.AddCors(options => {
        options.AddPolicy(name: "AllowLucinaOrigins",
            policy => {
                policy
                    .WithOrigins("https://www.lucinaravenwing.net",
                        "https://www.auth.lucinaravenwing.net",
                        "https://www.ygo.lucinaravenwing.net",
                        "https://lucinaravenwing.net",
                        "https://auth.lucinaravenwing.net",
                        "https://ygo.lucinaravenwing.net",
                        "http://localhost:3000",
                        "http://localhost:3000/")
                    .AllowAnyHeader()
                    .DisallowCredentials()
                    .AllowAnyMethod();
            });
    });

// Add services to the container.
    builder.Services.AddDbContext<AuthDbContext>(
        options => options.UseNpgsql(builder.Configuration.GetConnectionString("Auth"))
    );
    
    
// Add exception handler.
    builder.Services.AddMvc(options => {
        options.Filters.Add(new ExceptionHandlerFilterAttribute());
    });

// Add user defined services.
    builder.Services.AddScoped<IAuthService, AuthServiceImpl>();
    builder.Services.AddScoped<IEmailService, EmailServiceImpl>();
    builder.Services.AddScoped<IUserService, UserServiceImpl>();
    builder.Services.AddScoped<IRefreshTokenRepo, RefreshTokenRepo>();
    builder.Services.AddScoped<IAuthUserRepo, AuthUserRepo>();

// Set token validation params.
    TokenValidationParameters tokenValidationParameters = new() {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey =
            new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(builder.Configuration.GetSection("JWT:Secret").Value ?? string.Empty)),
        ValidateIssuer = true,
        ValidIssuer = builder.Configuration.GetSection("Jwt:Issuer").Value,
        ValidateAudience = true,
        ValidAudience = builder.Configuration.GetSection("Jwt:Audience").Value,

        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero
    };

    builder.Services.AddSingleton(tokenValidationParameters);

// Add Identity
    builder.Services.AddIdentity<AuthUser, IdentityRole<int>>().AddEntityFrameworkStores<AuthDbContext>()
        .AddDefaultTokenProviders();

// Add Authentication
    builder.Services.AddAuthentication(options => {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(options => {
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = tokenValidationParameters;
        });

// Add lockout attempts.
    builder.Services.Configure<IdentityOptions>(options => {
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(15);
        options.User.RequireUniqueEmail = true;
        options.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._+";
    });

//NLog: Setup NLog for Dependency Injection
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen(options => {
        options.SwaggerDoc(
            "v1", new OpenApiInfo {
                Version = "v1",
                Title = "LucinaRavenwing.net Auth API",
                Description =
                    "An ASP.NET Core Web API for authenticating on the *.lucinaravenwing.net domain.",
            });
        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme {
            In = ParameterLocation.Header,
            Description = "Please enter token",
            Name = "Authorization",
            Type = SecuritySchemeType.Http,
            BearerFormat = "JWT",
            Scheme = "bearer"
        });
        options.AddSecurityRequirement(new OpenApiSecurityRequirement {
            {
                new OpenApiSecurityScheme {
                    Reference = new OpenApiReference {
                        Type = ReferenceType.SecurityScheme,
                        Id = "Bearer"
                    }
                },
                new string[] { }
            }
        });


        string xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";

        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFileName));
    });

    WebApplication app = builder.Build();

// Configure the HTTP request pipeline.
    app.UseSwagger();
    app.UseSwaggerUI(swagger => {
        swagger.SwaggerEndpoint("/swagger/v1/swagger.json", "LucinaRavenwing.net Auth API");
        swagger.RoutePrefix = "api/swagger/v1";
    });

    app.UseRouting();
    
    app.UseCors(allowedOrigins);
    app.UseHttpsRedirection();

// Authentication & Authorization
    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

// Add roles to database.
    AuthDbInitializer.SeedRolesToDatabase(app).Wait();

    logger.Warn("Program started.");
    app.Run();
}
catch (Exception e) {
    logger.Error(e, "Program stopped due to exception in Program.cs.");
    throw;
}
finally {
    LogManager.Shutdown();
}