using System.Diagnostics.CodeAnalysis;
using AuthService.Controllers;
using AuthService.Data.Models;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using AuthService.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace AuthServiceTest.ControllerUnitTests;

public class UserControllerUnitTests {

    private Mock<IConfiguration> SetUpMockConfiguration() {
        Mock<IConfigurationSection> mockSection = new();
        mockSection.Setup(section => section.Value).Returns("this-is-a-key");

        Mock<IConfiguration> mockConfiguration = new();
        mockConfiguration.Setup(config => config.GetSection(It.IsAny<string>())).Returns(mockSection.Object);

        return mockConfiguration;
    }

    [Fact]
    public async void ConfirmEmailWithErrorsReturnsBadObject() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        EmailConfirmationRequestVm request = new() {
            ConfirmToken = "This is a token.",
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IUserService> mockUserService = new();

        mockUserService.Setup(service => service.ConfirmEmail(It.IsAny<EmailConfirmationRequestVm>()))
            .ReturnsAsync("Error: There was an error.");

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object, 
            mockConfiguration.Object);

        IActionResult response = await userController.ConfirmEmail(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: There was an error.", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ValidConfirmEmailReturnsOkResponse() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        EmailConfirmationRequestVm request = new() {
            ConfirmToken = "This is a token.",
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IUserService> mockUserService = new();

        mockUserService.Setup(service => service.ConfirmEmail(It.IsAny<EmailConfirmationRequestVm>()))
            .ReturnsAsync("Email confirmed.");

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ConfirmEmail(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Email confirmed.", responseConvert?.Value?.ToString());
    }
    
    [Fact]
    public async void ValidConfirmEmailWithRedirectReturnsRedirectResponse() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        EmailConfirmationRequestVm request = new() {
            ConfirmToken = "This is a token.",
            Email = "lucinaravenwing.net@gmail.com",
            Origin = "ygo.lucinaravenwing.net"
        };

        Mock<IUserService> mockUserService = new();

        mockUserService.Setup(service => service.ConfirmEmail(It.IsAny<EmailConfirmationRequestVm>()))
            .ReturnsAsync("Email confirmed.");

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ConfirmEmail(request);
        RedirectResult? responseConvert = response as RedirectResult;

        Assert.IsType<RedirectResult>(response);
        Assert.Contains("ygo.lucina", responseConvert?.Url);
    }

    [Fact]
    public async void ChangePasswordWithDifferentNewPasswordsReturnsBadObject() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        PasswordUpdateRequestVm request = new() {
            CurrentPassword = "SecurePassword",
            NewPassword = "MoreSecurePassword",
            ConfirmNewPassword = "NotSecurePassword"
        };

        UserController userController = new(new Mock<IUserService>().Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ChangePassword(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Updated passwords do not match.",
            responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ChangePasswordWithNullValuesReturnsServerError() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        PasswordUpdateRequestVm request = new() {
            CurrentPassword = "SecurePassword",
            NewPassword = "MoreSecurePassword",
            ConfirmNewPassword = "MoreSecurePassword"
        };

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(service => service.ChangePassword(It.IsAny<PasswordUpdateRequestVm>(),
                It.IsAny<string?>()))
            .ReturnsAsync(new EmailAuthResponseVm() {
                Username = null,
                Email = null,
                AuthResponse = new AuthResponseVm() {
                    Message = "Error: There was an error."
                }
            });

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ChangePassword(request);
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
        Assert.Contains("Error: Auth returned improper result.", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ChangePasswordWithErrorsReturnsBadObject() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        PasswordUpdateRequestVm request = new() {
            CurrentPassword = "SecurePassword",
            NewPassword = "MoreSecurePassword",
            ConfirmNewPassword = "MoreSecurePassword"
        };

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(service => service.ChangePassword(It.IsAny<PasswordUpdateRequestVm>(),
                It.IsAny<string?>()))
            .ReturnsAsync(new EmailAuthResponseVm() {
                AuthResponse = new AuthResponseVm() {
                    Message = "Error: There was an error."
                }
            });

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ChangePassword(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: There was an error.",
            (responseConvert?.Value as AuthResponseVm)?.Message);
    }

    [Fact]
    public async void ValidChangePasswordReturnsOkResponse() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        PasswordUpdateRequestVm request = new() {
            CurrentPassword = "SecurePassword",
            NewPassword = "MoreSecurePassword",
            ConfirmNewPassword = "MoreSecurePassword"
        };

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(service => service.ChangePassword(It.IsAny<PasswordUpdateRequestVm>(),
                It.IsAny<string?>()))
            .ReturnsAsync(new EmailAuthResponseVm() {
                Username = "lucinaravenwing",
                Email = "lucinaravenwing.net@gmail.com",
                AuthResponse = new AuthResponseVm() {
                    Message = "Password updated successfully."
                }
            });

        Mock<IEmailService> mockEmailService = new();
        mockEmailService.Setup(service =>
            service.SendPasswordChangeNoticeEmail(It.IsAny<string>(), It.IsAny<string>()));

        UserController userController = new(mockUserService.Object, mockEmailService.Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ChangePassword(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Password updated successfully.",
            (responseConvert?.Value as AuthResponseVm)?.Message);
    }

    [Fact]
    public async void ChangeUsernameWithInvalidUsernameReturnsBadObject() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        UsernameUpdateRequestVm request = new() {
            UpdatedUsername = "lucy@",
            UserId = "1"
        };

        UserController userController = new(new Mock<IUserService>().Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ChangeUserName(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: Username must be 6", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ChangeUsernameWithNullReturnsReturnsBadObject() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        UsernameUpdateRequestVm request = new() {
            UpdatedUsername = "lucinaravenwing",
            UserId = "1"
        };

        Mock<IUserService> mockUserService = new();

        mockUserService.Setup(service => service.ChangeUserName(It.IsAny<UsernameUpdateRequestVm>()))
            .ReturnsAsync(new EmailAuthResponseVm() {
                Email = null,
                Username = null,
                AuthResponse = null
            });

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ChangeUserName(request);
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Contains("Error: Result returned", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ChangeUsernameWithErrorsReturnsBadObject() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        UsernameUpdateRequestVm request = new() {
            UpdatedUsername = "lucinaravenwing",
            UserId = "1"
        };

        Mock<IUserService> mockUserService = new();

        mockUserService.Setup(service => service.ChangeUserName(It.IsAny<UsernameUpdateRequestVm>()))
            .ReturnsAsync(new EmailAuthResponseVm() {
                AuthResponse = new AuthResponseVm() {
                    Message = "Error: There were errors."
                }
            });

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ChangeUserName(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: There were", (responseConvert?.Value as AuthResponseVm)?.Message);
    }

    [Fact]
    public async void ValidChangeUsernameReturnsOkResponse() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        UsernameUpdateRequestVm request = new() {
            UpdatedUsername = "lucinaravenwing",
            UserId = "1"
        };

        Mock<IUserService> mockUserService = new();

        mockUserService.Setup(service => service.ChangeUserName(It.IsAny<UsernameUpdateRequestVm>()))
            .ReturnsAsync(new EmailAuthResponseVm() {
                Username = "lucinaravenwing",
                Email = "lucinaravenwing.net@gmail.com",
                AuthResponse = new AuthResponseVm() {
                    Message = "Username changed."
                }
            });

        Mock<IEmailService> mockEmailService = new();

        mockEmailService.Setup(service => service.SendUsernameChangeNoticeEmail(It.IsAny<string>(),
            It.IsAny<string>(), It.IsAny<string>()));

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ChangeUserName(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Username changed.", (responseConvert?.Value as AuthResponseVm)?.Message);
    }

    [Fact]
    public async void ResetPasswordWithErrorsReturnsBadObject() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        PasswordResetRequestVm request = new() {
            Email = "lucinaravenwing.net@gmail.com",
            NewPassword = "SuperSecurePassword",
            Token = "ThisIsAToken"
        };

        Mock<IUserService> mockUserService = new();

        mockUserService.Setup(service => service.ResetPassword(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(new AuthResponseVm() {
                Message = "Error: There were errors."
            });

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ResetPassword(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: There were", (responseConvert?.Value as AuthResponseVm)?.Message);
    }

    [Fact]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public async void ValidResetPasswordWithNoOriginReturnsOkResponse() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        PasswordResetRequestVm request = new() {
            Email = "lucinaravenwing.net@gmail.com",
            NewPassword = "SuperSecurePassword",
            Token = "ThisIsAToken"
        };

        Mock<IUserService> mockUserService = new();

        mockUserService.Setup(service => service.ResetPassword(It.IsAny<string>(), It.IsAny<string>(),
                It.IsAny<string>()))
            .ReturnsAsync(new AuthResponseVm() {
                Token = "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg",
                Message = "Password reset."
            });

        Mock<IEmailService> mockEmailService = new();

        mockEmailService.Setup(service =>
            service.SendPasswordChangeNoticeEmail(It.IsAny<string>(), It.IsAny<string>()));

        UserController userController = new(mockUserService.Object, mockEmailService.Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ResetPassword(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Password reset.", (responseConvert?.Value as AuthResponseVm)?.Message);
    }

    [Fact]
    public async void ResendConfirmationEmailWithErrorsReturnsBadObject() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        ResendConfirmEmailRequestVm request = new() {
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IUserService> mockUserService = new();
        
        mockUserService.Setup(service => service.ResendConfirmEmail(It.IsAny<ResendConfirmEmailRequestVm>()))
            .ReturnsAsync(new Tuple<string, AuthUser>("Error: There were errors.", new AuthUser()));

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.ResendConfirmationEmail(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: There were errors.", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ResendConfirmationEmailWithNullConfirmUrlReturnsBadRequest() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        ResendConfirmEmailRequestVm request = new() {
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IUserService> mockUserService = new();
        
        mockUserService.Setup(service => service.ResendConfirmEmail(It.IsAny<ResendConfirmEmailRequestVm>()))
            .ReturnsAsync(new Tuple<string, AuthUser>("Confirmation Token generated.", new AuthUser()));

        mockUserService.Setup(service => service.GenerateUrl(It.IsAny<IUrlHelper>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<object>(), It.IsAny<string>()))
            .Returns((string?)null);

        Mock<IUrlHelper> mockUrlHelper = new();

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        userController.Url = mockUrlHelper.Object;

        IActionResult response = await userController.ResendConfirmationEmail(request);
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
        Assert.Contains("Error: Confirmation email", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ValidResendConfirmationEmailReturnsOkResponse() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        ResendConfirmEmailRequestVm request = new() {
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IUserService> mockUserService = new();
        
        mockUserService.Setup(service => service.ResendConfirmEmail(It.IsAny<ResendConfirmEmailRequestVm>()))
            .ReturnsAsync(new Tuple<string, AuthUser>("ThisIsAConfirmationToken", new AuthUser()));

        mockUserService.Setup(service => service.GenerateUrl(It.IsAny<IUrlHelper>(), It.IsAny<string>(),
                It.IsAny<string>(), It.IsAny<object>(), It.IsAny<string>()))
            .Returns("ThisIsAUrl");

        Mock<IEmailService> mockEmailService = new();

        mockEmailService.Setup(service => service.SendConfirmationEmail(It.IsAny<AuthUser>(), It.IsAny<string>()));

        Mock<IUrlHelper> mockUrlHelper = new();

        UserController userController = new(mockUserService.Object, mockEmailService.Object,
            mockConfiguration.Object);

        userController.Url = mockUrlHelper.Object;

        IActionResult response = await userController.ResendConfirmationEmail(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Confirmation email resent successfully.", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void GetUserDataWithIncorrectKeyReturnsErrorMessage() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        UserDataRequestVm request = new() {
            Key = "this-is-the-wrong-key"
        };

        UserController userController = new(new Mock<IUserService>().Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.GetUserData(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Key is incorrect.", (responseConvert?.Value as UserDataReturnVm)?.Message);
    }
    
    [Fact]
    public async void GetUserDataWithCorrectKeyAndBlankUsernameAndEmailReturnsErrorMessage() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        UserDataRequestVm request = new() {
            Key = "this-is-a-key",
            Username = "",
            Email = ""
        };

        UserController userController = new(new Mock<IUserService>().Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.GetUserData(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Please send either a username or email", (responseConvert?.Value as UserDataReturnVm)?.Message);
    }
    
    [Fact]
    public async void GetUserDataWithBlankReturnsFromUserServiceReturnErrorMessage() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        UserDataRequestVm request = new() {
            Key = "this-is-a-key",
            Username = "aura"
        };

        UserDataReturnVm userData = new() {
            Message = ""
        };

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(service => service.GetUserData(It.IsAny<UserDataRequestVm>()))
            .ReturnsAsync(userData);

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.GetUserData(request);
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);

        userData.Message = "Hi";
        userData.Username = "username";
        userData.Email = "";

        response = await userController.GetUserData(request);
        responseConvert = response as ObjectResult;
        
        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);

        userData.Email = "email";
        userData.Username = "";
        
        response = await userController.GetUserData(request);
        responseConvert = response as ObjectResult;
        
        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
    }
    
    [Fact]
    public async void ValidGetUserDataReturnsProperResponse() {
        Mock<IConfiguration> mockConfiguration = SetUpMockConfiguration();

        UserDataRequestVm request = new() {
            Key = "this-is-a-key",
            Username = "username",
            Email = ""
        };

        Mock<IUserService> mockUserService = new();
        mockUserService.Setup(service => service.GetUserData(It.IsAny<UserDataRequestVm>()))
            .ReturnsAsync(new UserDataReturnVm() {
                Message = "Good!",
                Username = "username",
                Email = "email@email.com"
            });

        UserController userController = new(mockUserService.Object, new Mock<IEmailService>().Object,
            mockConfiguration.Object);

        IActionResult response = await userController.GetUserData(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Good!", (responseConvert?.Value as UserDataReturnVm)?.Message);
    }
}