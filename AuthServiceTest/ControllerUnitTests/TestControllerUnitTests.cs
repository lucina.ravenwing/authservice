using AuthService.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace AuthServiceTest.ControllerUnitTests; 

public class TestControllerUnitTests {
    [Fact]
    public void TestPublicReturnsOkResponse() {
        TestController testController = new();

        IActionResult response= testController.TestPublic();
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("public Test API.", responseConvert?.Value?.ToString());
    }
    
    [Fact]
    public void TestUserReturnsOkResponse() {
        TestController testController = new();

        IActionResult response= testController.TestUser();
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("user Test API.", responseConvert?.Value?.ToString());
    }
    
    [Fact]
    public void TestModeratorReturnsOkResponse() {
        TestController testController = new();

        IActionResult response= testController.TestModerator();
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("moderator Test API.", responseConvert?.Value?.ToString());
    }
    
    [Fact]
    public void TestAdminReturnsOkResponse() {
        TestController testController = new();

        IActionResult response= testController.TestAdmin();
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("admin Test API.", responseConvert?.Value?.ToString());
    }
}