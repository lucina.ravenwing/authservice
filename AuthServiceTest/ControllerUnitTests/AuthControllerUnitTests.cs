using AuthService.Controllers;
using AuthService.Data.DTO;
using AuthService.Data.Models;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using AuthService.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Moq;
using Xunit;

namespace AuthServiceTest.ControllerUnitTests;

public class AuthControllerUnitTests {
    [Fact]
    public async void SignUpWithInvalidStateReturnsBadRequest() {
        AuthController authController = new(new Mock<IAuthService>().Object, new Mock<IEmailService>().Object);
         SignUpRequestVm request = new();
        authController.ModelState.AddModelError("Username", "Username is a required field.");

        IActionResult response= await authController.SignUp(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: Please provide", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void SignUpWithDifferingPasswordAndConfirmPasswordReturnsBadRequest() {
        AuthController authController = new(new Mock<IAuthService>().Object, new Mock<IEmailService>().Object);
         SignUpRequestVm request = new() {
            EmailAddress = "lucinaravenwing.net@gmail.com",
            Username = "LucinaRaven",
            Password = "ThisIsAPassword",
            ConfirmPassword = "ThisIsADifferentPassword"
        };

        IActionResult response= await authController.SignUp(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: Passwords", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void SignUpWithIncorrectUsernameInputReturnsBadRequest() {
        AuthController authController = new(new Mock<IAuthService>().Object, new Mock<IEmailService>().Object);
         SignUpRequestVm request = new() {
            Username = "Luci"
        };

        IActionResult response= await authController.SignUp(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: Username must be ", responseConvert?.Value?.ToString());

        request = new SignUpRequestVm() {
            Username = "LucinaRaven@"
        };

        response = await authController.SignUp(request);
        responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("cannot contain @", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void SignUpWithErrorResultReturnsBadRequestWithErrors() {
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.SignUpAsync(It.IsAny<SignUpRequestVm>()))
            .ReturnsAsync(new SignUpDto() {
                Message = "Error: There was an error. :("
            });

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);
        // Since this is mocked, any values work.
         SignUpRequestVm request = new() {
            Username = "LucinaRavenwing"
        };

        IActionResult response= await authController.SignUp(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error:", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void SignUpWithNoErrorResultButNullConfirmTokenReturnsBadRequest() {
        Mock<IUrlHelper> urlMock = new();

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.SignUpAsync(It.IsAny<SignUpRequestVm>()))
            .ReturnsAsync(new SignUpDto() {
                Message = "Success. :)",
                NewUser = new AuthUser() {
                    UserName = "LucinaRavenwing",
                    Email = "lucinaravenwing.net@gmail.com"
                },
                ConfirmToken = "ThisIsAConfirmToken"
            });

        mockAuthService.Setup(service => service.GenerateUrl(It.IsAny<IUrlHelper>(), It.IsAny<string>(),
            It.IsAny<string>(), It.IsAny<object>(), It.IsAny<string>())).Returns((string?)null);

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);
        authController.Url = urlMock.Object;

        // Since this is mocked, any values work.
         SignUpRequestVm request = new() {
            Username = "LucinaRavenwing"
        };

        IActionResult response= await authController.SignUp(request);
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
        Assert.Contains("Error: Confirmation", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ValidSignUpReturnsOkResult() {
        Mock<IUrlHelper> mockUrl = new();
        Mock<IAuthService> mockAuthService = new();
        Mock<IEmailService> mockEmailService = new();
        
        mockAuthService.Setup(service => service.SignUpAsync(It.IsAny<SignUpRequestVm>()))
            .ReturnsAsync(new SignUpDto() {
                Message = "Success. :)",
                NewUser = new AuthUser() {
                    UserName = "LucinaRavenwing",
                    Email = "lucinaravenwing.net@gmail.com"
                },
                ConfirmToken = "ThisIsAConfirmToken"
            });
        mockAuthService.Setup(service => service.GenerateUrl(It.IsAny<IUrlHelper>(), It.IsAny<string>(),
            It.IsAny<string>(), It.IsAny<object>(), It.IsAny<string>())).Returns("URL string");

        mockEmailService.Setup(service => service.SendConfirmationEmail(It.IsAny<AuthUser>(), It.IsAny<string>()));
        
        AuthController authController = new(mockAuthService.Object, mockEmailService.Object);
        authController.Url = mockUrl.Object;

        // Since this is mocked, any values work.
         SignUpRequestVm request = new() {
            Username = "LucinaRavenwing"
        };

        IActionResult response= await authController.SignUp(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Success. :)", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void LoginWithMissingUserNamePasswordOrIncorrectCredentialsReturnsBadRequest() {
        LoginRequestVm request1 = new() {
            UserNameEmail = ""
        };

        LoginRequestVm request2 = new() {
            UserNameEmail = "lucinaravenwing"
        };
        
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.LoginAsync(request1))
            .ReturnsAsync(new AuthResponseVm() {
                Message = "Error: Please enter"
            });

        mockAuthService.Setup(service => service.LoginAsync(request2))
            .ReturnsAsync(new AuthResponseVm() {
                Message = "Error: Username, email"
            });

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);

        IActionResult response= await authController.Login(request1);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: Please enter", (responseConvert?.Value as AuthResponseVm)?.Message);

        response = await authController.Login(request2);
        responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: Username,", (responseConvert?.Value as AuthResponseVm)?.Message);
    }
    
    [Fact]
    public async void LoginWithUnconfirmedEmailReturns401() {
        LoginRequestVm request1 = new() {
            UserNameEmail = ""
        };

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.LoginAsync(request1))
            .ReturnsAsync(new AuthResponseVm() {
                Message = "Error: not been confirmed"
            });

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);

        IActionResult response= await authController.Login(request1);
        UnauthorizedObjectResult? responseConvert = response as UnauthorizedObjectResult;

        Assert.IsType<UnauthorizedObjectResult>(response);
        Assert.Equal(401, responseConvert?.StatusCode);
    }

    [Fact]
    public async void ValidLoginReturnsOkResult() {
        LoginRequestVm request = new() {
            UserNameEmail = "lucinaravenwing",
            Password = "FakePassword"
        };

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.LoginAsync(request))
            .ReturnsAsync(new AuthResponseVm() {
                Message = "Token created successfully!"
            });

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);

        IActionResult response= await authController.Login(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Token created", (responseConvert?.Value as AuthResponseVm)?.Message);
    }

    [Fact]
    public void LogoutWithInvalidModelStateReturnsBadRequest() {
        LogoutRequestVm request = new() {
            RefreshToken = "ThisIsAToken"
        };

        AuthController authController = new(new Mock<IAuthService>().Object, new Mock<IEmailService>().Object);

        authController.ModelState.AddModelError("Invalid input.", "Refresh Token is a required field.");
        
        IActionResult response= authController.Logout(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Please provide ref", (responseConvert?.Value?.ToString()));
    }

    [Fact]
    public void ValidLogoutReturnsOkResult() {
        LogoutRequestVm request = new() {
            RefreshToken = "ThisIsAToken"
        };

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.LogoutAsync(request));

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);

        IActionResult response= authController.Logout(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("User logged out successfully.", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ForgotPasswordWithErrorReturnedFromServiceReturnsBadRequest() {
        // Assert.Fail("Not implemented.");
        
        ForgotPasswordRequestVm request = new() {
            UserNameEmail = "lucinaravenwing"
        };
        
        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.ForgotPasswordAsync(It.IsAny<ForgotPasswordRequestVm>()))
            .ReturnsAsync(new Tuple<string, AuthUser>("Error: There was an error.", new AuthUser()));

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);

        authController.ModelState.AddModelError("Invalid input.", "Refresh Token is a required field.");
        
        IActionResult response= await authController.ForgotPassword(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: There was", (responseConvert?.Value?.ToString()));
    }

    [Fact]
    public async void ForgotPasswordWithNullEmailTokenUrlReturnsServerError() {
        Mock<IUrlHelper> urlMock = new();
        
        // Since this is mocked, any values work.
        ForgotPasswordRequestVm request = new() {
            UserNameEmail = "lucinaravenwing"
        };

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.ForgotPasswordAsync(It.IsAny<ForgotPasswordRequestVm>()))
            .ReturnsAsync(new Tuple<string, AuthUser>("User created successfully.", new AuthUser() {
                Email = "lucinaravenwing.net@gmail.com"
            }));

        mockAuthService.Setup(service => service.GenerateUrl(It.IsAny<IUrlHelper>(), It.IsAny<string>(),
            It.IsAny<string>(), It.IsAny<object>(), It.IsAny<string>())).Returns((string?)null);

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);
        authController.Url = urlMock.Object;

        IActionResult response= await authController.ForgotPassword(request);
        ObjectResult? responseConvert = response as ObjectResult;

        Assert.IsType<ObjectResult>(response);
        Assert.Equal(500, responseConvert?.StatusCode);
        Assert.Contains("Error: Password reset", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void ValidForgotPasswordReturnsOkResponse() {
        // Assert.Fail("Not implemented.");
        
        Mock<IUrlHelper> urlMock = new();
        
        // Since this is mocked, any values work.
        ForgotPasswordRequestVm request = new() {
            UserNameEmail = "lucinaravenwing"
        };

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.ForgotPasswordAsync(It.IsAny<ForgotPasswordRequestVm>()))
            .ReturnsAsync(new Tuple<string, AuthUser>("User created successfully.", new AuthUser() {
                Email = "lucinaravenwing.net@gmail.com"
            }));

        mockAuthService.Setup(service => service.GenerateUrl(It.IsAny<IUrlHelper>(), It.IsAny<string>(),
            It.IsAny<string>(), It.IsAny<object>(), It.IsAny<string>())).Returns("This is a url.");

        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);
        authController.Url = urlMock.Object;

        IActionResult response= await authController.ForgotPassword(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Password reset email sent.", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void RefreshTokenWithBadModelStateReturnsBadResponse() {
        TokenRequestVm request = new() {
            RefreshToken = "This is a refresh token.",
            Token = "This is an access token."
        };
        
        AuthController authController = new(new Mock<IAuthService>().Object, new Mock<IEmailService>().Object);
        authController.ModelState.AddModelError("Invalid Tokens", "Tokens were invalid.");

        IActionResult response= await authController.RefreshToken(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Please provide access", responseConvert?.Value?.ToString());
    }

    [Fact]
    public async void RefreshTokenWithErrorsReturnedFromServiceReturnsBadResponse() {
        TokenRequestVm request = new() {
            RefreshToken = "This is a refresh token.",
            Token = "This is an access token."
        };

        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(service => service.ValidateAndGenerateTokenAsync(It.IsAny<SecurityTokenHandler>(), It.IsAny<TokenRequestVm>()))
            .ReturnsAsync(new AuthResponseVm() {
                Message = "Error: There was an error."
            });
        
        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);
        
        IActionResult response= await authController.RefreshToken(request);
        BadRequestObjectResult? responseConvert = response as BadRequestObjectResult;

        Assert.IsType<BadRequestObjectResult>(response);
        Assert.Contains("Error: There was", (responseConvert?.Value as AuthResponseVm)?.Message);
    }

    [Fact]
    public async void ValidRefreshTokenReturnsOkResponse() {
        TokenRequestVm request = new() {
            RefreshToken = "This is a refresh token.",
            Token = "This is an access token."
        };

        Mock<IAuthService> mockAuthService = new();

        mockAuthService.Setup(service => service.ValidateAndGenerateTokenAsync( It.IsAny<SecurityTokenHandler>(), It.IsAny<TokenRequestVm>()))
            .ReturnsAsync(new AuthResponseVm() {
                Message = "Refresh token generated."
            });
        
        AuthController authController = new(mockAuthService.Object, new Mock<IEmailService>().Object);
        
        IActionResult response= await authController.RefreshToken(request);
        OkObjectResult? responseConvert = response as OkObjectResult;

        Assert.IsType<OkObjectResult>(response);
        Assert.Contains("Refresh token", (responseConvert?.Value as AuthResponseVm)?.Message);
    }
}