using Microsoft.AspNetCore.Identity;
using Moq;

namespace AuthServiceTest.Helpers; 

public static class MockUserManagerHelper {

    public static Mock<UserManager<TUser>> MockUserManager<TUser>() where TUser : class {
        Mock<IUserStore<TUser>> mockStore = new();
        Mock<UserManager<TUser>> mockManager =
            new(mockStore.Object, null, null, null, null, null, null, null, null);
        mockManager.Object.UserValidators.Add(new UserValidator<TUser>());
        mockManager.Object.PasswordValidators.Add(new PasswordValidator<TUser>());

        return mockManager;

    }

}