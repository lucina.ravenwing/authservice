using AuthService.Data.Models;
using AuthService.Data.Repos;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using AuthService.Services.Implementations;
using AuthService.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using Moq;
using Xunit;

namespace AuthServiceTest.ServiceUnitTests.ImplementationsUnitTests;

public class UserServiceImplUnitTests {
    [Fact]
    public async void ConfirmEmailWithNullUserReturnedReturnsError() {
        EmailConfirmationRequestVm request = new() {
            ConfirmToken = "ThisIsAConfirmationToken",
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        string response = await testUserService.ConfirmEmail(request);

        Assert.Contains("Error: User with given username", response);
    }

    [Fact]
    public async void ConfirmEmailWithFailureReturnsError() {
        EmailConfirmationRequestVm request = new() {
            ConfirmToken = "ThisIsAConfirmationToken",
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.ConfirmEmailAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(IdentityResult.Failed());

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        string response = await testUserService.ConfirmEmail(request);

        Assert.Contains("Email confirmation failed. Errors:", response);
    }

    [Fact]
    public async void ProperConfirmEmailReturnsSuccess() {
        EmailConfirmationRequestVm request = new() {
            ConfirmToken = "ThisIsAConfirmationToken",
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.ConfirmEmailAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(IdentityResult.Success);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        string response = await testUserService.ConfirmEmail(request);

        Assert.Contains("Email confirmed ", response);
    }

    [Fact]
    public async void ResendConfirmEmailWithNullUserReturnedReturnsError() {
        ResendConfirmEmailRequestVm request = new() {
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        Tuple<string, AuthUser> response = await testUserService.ResendConfirmEmail(request);

        Assert.Contains("Error: User with given email", response.Item1);
    }

    [Fact]
    public async void ResentConfirmEmailWithAlreadyConfirmedUserReturnsError() {
        ResendConfirmEmailRequestVm request = new() {
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser { EmailConfirmed = true });

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        Tuple<string, AuthUser> response = await testUserService.ResendConfirmEmail(request);

        Assert.Contains("Error: Email for user already", response.Item1);
    }

    [Fact]
    public async void ProperResendConfirmEmailReturnsSuccess() {
        ResendConfirmEmailRequestVm request = new() {
            Email = "lucinaravenwing.net@gmail.com"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.GenerateEmailConfirmationTokenAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync("ThisIsAConfirmEmail.");

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        Tuple<string, AuthUser> response = await testUserService.ResendConfirmEmail(request);

        Assert.Equal("ThisIsAConfirmEmail.", response.Item1);
    }

    [Fact]
    public async void ChangePasswordWithNullUserIdReturnsError() {
        PasswordUpdateRequestVm request = new() {
            UserId = "1",
            CurrentPassword = "Password_1",
            NewPassword = "Password_2",
            ConfirmNewPassword = "Password_2"
        };

        UserServiceImpl testUserService = new(new Mock<IAuthUserRepo>().Object, new Mock<IAuthService>().Object);

        EmailAuthResponseVm response = await testUserService.ChangePassword(request, null);

        Assert.Contains("Error: Calling user cannot", response.AuthResponse?.Message);
    }

    [Fact]
    public async void ChangePasswordWithNullUserReturnedReturnsError() {
        PasswordUpdateRequestVm request = new() {
            UserId = "1",
            CurrentPassword = "Password_1",
            NewPassword = "Password_2",
            ConfirmNewPassword = "Password_2"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        EmailAuthResponseVm response = await testUserService.ChangePassword(request, "1");

        Assert.Contains("Error: User not found.", response.AuthResponse?.Message);
    }

    [Fact]
    public async void ChangePasswordWithFailureReturnsError() {
        PasswordUpdateRequestVm request = new() {
            UserId = "1",
            CurrentPassword = "Password_1",
            NewPassword = "Password_2",
            ConfirmNewPassword = "Password_2"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo =>
                repo.ChangePasswordAsync(It.IsAny<AuthUser>(), It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(IdentityResult.Failed(new IdentityError(){Code = "Error", Description = "There was an error."}));

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        EmailAuthResponseVm response = await testUserService.ChangePassword(request, "1");

        Assert.Contains("Error: Password could not be updated.", response.AuthResponse?.Message);
    }

    [Fact]
    public async void ProperChangePasswordReturnsSuccess() {
        PasswordUpdateRequestVm request = new() {
            UserId = "1",
            CurrentPassword = "Password_1",
            NewPassword = "Password_2",
            ConfirmNewPassword = "Password_2"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo =>
                repo.ChangePasswordAsync(It.IsAny<AuthUser>(), It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(IdentityResult.Success);

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.RegenerateTokensAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new AuthResponseVm { Message = "ThisIsAToken" });

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, mockAuthService.Object);

        EmailAuthResponseVm response = await testUserService.ChangePassword(request, "1");

        Assert.Equal("Password successfully updated.", response.AuthResponse?.Message);
    }

    [Fact]
    public async void ChangeUserNameWithNullUserReturnedReturnsError() {
        UsernameUpdateRequestVm request = new() {
            UserId = "1",
            UpdatedUsername = "lucinaravenwing"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        EmailAuthResponseVm response = await testUserService.ChangeUserName(request);

        Assert.Contains("Error: User not found.", response.AuthResponse?.Message);
    }

    [Fact]
    public async void ChangeUserNameWithFailureReturnsError() {
        UsernameUpdateRequestVm request = new() {
            UserId = "1",
            UpdatedUsername = "lucinaravenwing"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.NormalizeName(It.IsAny<string>()))
            .Returns(request.UpdatedUsername.ToUpper);
        mockAuthUserRepo.Setup(repo => repo.UpdateAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(IdentityResult.Failed(new IdentityError(){Code = "Error", Description = "There was an error."}));

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.RegenerateTokensAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new AuthResponseVm { Message = "ThisIsAToken" });

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, mockAuthService.Object);

        EmailAuthResponseVm response = await testUserService.ChangeUserName(request);

        Assert.Contains("Error: Username could not be updated.", response.AuthResponse?.Message);
    }

    [Fact]
    public async void ProperChangeUserNameReturnsSuccess() {
        UsernameUpdateRequestVm request = new() {
            UserId = "1",
            UpdatedUsername = "lucinaravenwing"
        };

        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.NormalizeName(It.IsAny<string>()))
            .Returns(request.UpdatedUsername.ToUpper);
        mockAuthUserRepo.Setup(repo => repo.UpdateAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(IdentityResult.Success);

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.RegenerateTokensAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new AuthResponseVm { Message = "ThisIsAToken" });

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, mockAuthService.Object);

        EmailAuthResponseVm response = await testUserService.ChangeUserName(request);

        Assert.Equal("Username successfully updated.", response.AuthResponse?.Message);
    }

    [Fact]
    public async void ResetPasswordWithNullUserReturnsError() {
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        AuthResponseVm response =
            await testUserService.ResetPassword("lucinaravenwing.net@gmail.com", "Password_2", "ThisIsAConfirmToken");

        Assert.Contains("Error: User not found.", response.Message);
    }

    [Fact]
    public async void ResetPasswordWithFailureReturnsError() {
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.ResetPasswordAsync(It.IsAny<AuthUser>(), It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(IdentityResult.Failed(new IdentityError(){Code = "Error", Description = "There was an error."}));

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.RegenerateTokensAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new AuthResponseVm { Message = "ThisIsAToken" });

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, mockAuthService.Object);

        AuthResponseVm response =
            await testUserService.ResetPassword("lucinaravenwing.net@gmail.com", "Password_2", "ThisIsAConfirmToken");


        Assert.Contains("Error: Password could not be reset.", response.Message);
    }

    [Fact]
    public async void ProperResetPasswordReturnsSuccess() {
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.ResetPasswordAsync(It.IsAny<AuthUser>(), It.IsAny<string>(), It.IsAny<string>()))
            .ReturnsAsync(IdentityResult.Success);

        Mock<IAuthService> mockAuthService = new();
        mockAuthService.Setup(service => service.RegenerateTokensAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new AuthResponseVm { Message = "ThisIsAToken" });

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, mockAuthService.Object);

        AuthResponseVm response =
            await testUserService.ResetPassword("lucinaravenwing.net@gmail.com", "Password_2", "ThisIsAConfirmToken");
        
        Assert.Equal("ThisIsAToken", response.Message);
    }

    [Fact]
    public async void GetUserDataWithNullUserReturnsErrorMessage() {
        AuthUser? returnUser = null;

        UserDataRequestVm request = new() {
            Email = "",
            Username = "username"
        };
        
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(returnUser);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        UserDataReturnVm response = await testUserService.GetUserData(request);

        Assert.Contains("Error: User not found.", response.Message);
    }
    
    [Fact]
    public async void GetUserDataWithInvalidUserReturnsErrorMessage() {
        AuthUser returnUser = new() {
            UserName = "",
            Email = ""
        };

        UserDataRequestVm request = new() {
            Email = "email",
            Username = ""
        };
        
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync(returnUser);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        UserDataReturnVm response = await testUserService.GetUserData(request);

        Assert.Contains("Error: User found but Username", response.Message);

        returnUser.UserName = "username";
        
        response = await testUserService.GetUserData(request);

        Assert.Contains("Error: User found but Email", response.Message);
    }

    [Fact]
    public async void ProperGetUserDataReturnsGoodResponse() {
        AuthUser returnUser = new() {
            Email = "email",
            UserName = "username"
        };

        UserDataRequestVm request = new() {
            Email = "",
            Username = "username"
        };
        
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(returnUser);

        UserServiceImpl testUserService = new(mockAuthUserRepo.Object, new Mock<IAuthService>().Object);

        UserDataReturnVm response = await testUserService.GetUserData(request);

        Assert.Contains("User found successfully.", response.Message);

        Assert.Equal("email", response.Email);
        Assert.Equal("username", response.Username);
    }
}