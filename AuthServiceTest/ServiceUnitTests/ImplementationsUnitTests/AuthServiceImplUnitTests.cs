using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using AuthService.Data.DTO;
using AuthService.Data.Models;
using AuthService.Data.Repos;
using AuthService.Data.Requests;
using AuthService.Data.Responses;
using AuthService.Services.Implementations;
using AuthServiceTest.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Moq;
using Xunit;

namespace AuthServiceTest.ServiceUnitTests.ImplementationsUnitTests;

public class AuthServiceImplUnitTests {
    [Fact]
    public async void SignUpAsyncWithInvalidPasswordReturnsErrorSignUpDto() {
        SignUpRequestVm request = new() {
            Password = "NotGood",
            ConfirmPassword = "NotGood"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();

        AuthServiceImpl testAuthService = new(new Mock<IAuthUserRepo>().Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        SignUpDto response = await testAuthService.SignUpAsync(request);

        Assert.Contains("Error: Password does not meet", response.Message);
    }

    [Fact]
    public async void SignUpAsyncWithExistingEmailOrUsernameReturnsErrorSignUpDto() {
        SignUpRequestVm request = new() {
            Username = "ExistingUsername",
            EmailAddress = "ExistingEmail",
            Password = "GoodPassword_1",
            ConfirmPassword = "GoodPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync("ExistingEmail"))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync("NewEmail"))
            .ReturnsAsync((AuthUser?)null);
        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync("ExistingUsername"))
            .ReturnsAsync(new AuthUser());

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        SignUpDto response = await testAuthService.SignUpAsync(request);

        Assert.Contains("with given email", response.Message);

        request.EmailAddress = "NewEmail";

        response = await testAuthService.SignUpAsync(request);

        Assert.Contains("with given username", response.Message);
    }

    [Fact]
    public async void SignUpAsyncWithErrorCreatingUserReturnsErrorSignUpDto() {
        SignUpRequestVm request = new() {
            Username = "NewUsername",
            EmailAddress = "NewEmail",
            Password = "GoodPassword_1",
            ConfirmPassword = "GoodPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        IdentityResult result = IdentityResult.Failed(new IdentityError() {
            Description = "Error: There was an error.",
            Code = "ThisErrored."
        });

        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync("NewEmail"))
            .ReturnsAsync((AuthUser?)null);
        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync("NewUsername"))
            .ReturnsAsync((AuthUser?)null);
        mockAuthUserRepo.Setup(repo => repo.CreateAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(result);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        SignUpDto response = await testAuthService.SignUpAsync(request);

        Assert.Contains("Error: User was not able to be created.", response.Message);
    }

    [Fact]
    public async void ValidSignUpAsyncReturnsFilledSignUpDto() {
        SignUpRequestVm request = new() {
            Username = "NewUsername",
            EmailAddress = "NewEmail",
            Password = "GoodPassword_1",
            ConfirmPassword = "GoodPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        IdentityResult result = IdentityResult.Success;

        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync("NewEmail"))
            .ReturnsAsync((AuthUser?)null);
        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync("NewUsername"))
            .ReturnsAsync((AuthUser?)null);
        mockAuthUserRepo.Setup(repo => repo.CreateAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(result);
        mockAuthUserRepo.Setup(repo => repo.AddToRoleAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(result);
        mockAuthUserRepo.Setup(repo => repo.GenerateEmailConfirmationTokenAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync("ThisIsAConfirmToken.");

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        SignUpDto response = await testAuthService.SignUpAsync(request);

        Assert.Contains("User created successfully!", response.Message);
        Assert.Equal("NewUsername", response.NewUser.UserName);
        Assert.Equal("ThisIsAConfirmToken.", response.ConfirmToken);
    }

    [Fact]
    public async void LoginAsyncWithBlankUserNameEmailReturnsErrorAuthResponse() {
        LoginRequestVm request = new() {
            UserNameEmail = "",
            Password = "GoodPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();

        AuthServiceImpl testAuthService = new(new Mock<IAuthUserRepo>().Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        AuthResponseVm response = await testAuthService.LoginAsync(request);

        Assert.Contains("Error: Please enter", response.Message);
    }

    [Fact]
    public async void LoginAsyncWithNonExistentAuthUserReturnsErrorAuthResponse() {
        LoginRequestVm request = new() {
            UserNameEmail = "ValidUsername",
            Password = "GoodPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        AuthResponseVm response = await testAuthService.LoginAsync(request);

        Assert.Contains("Error: Username, email", response.Message);

        request = new LoginRequestVm() {
            UserNameEmail = "ValidUsername@valid.com",
            Password = "GoodPassword_1"
        };

        response = await testAuthService.LoginAsync(request);

        Assert.Contains("Error: Username, email", response.Message);
    }

    [Fact]
    public async void LoginAsyncWithExistingNonValidatedUserReturnsErrorAuthResponse() {
        LoginRequestVm request = new() {
            UserNameEmail = "ValidUsername",
            Password = "GoodPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.IsEmailConfirmedAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(false);
        mockAuthUserRepo.Setup(repo => repo.AccessFailedAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(IdentityResult.Success);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        AuthResponseVm response = await testAuthService.LoginAsync(request);

        Assert.Contains("Error: Email has not been", response.Message);
    }

    [Fact]
    public async void LoginAsyncWithExistingUserButInvalidPasswordReturnsErrorAuthResponse() {
        LoginRequestVm request = new() {
            UserNameEmail = "ValidUsername",
            Password = "GoodPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.IsEmailConfirmedAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(true);
        mockAuthUserRepo.Setup(repo => repo.AccessFailedAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(IdentityResult.Success);
        mockAuthUserRepo.Setup(repo => repo.CheckPasswordAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(false);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        AuthResponseVm response = await testAuthService.LoginAsync(request);

        Assert.Contains("Error: Username, email, or password", response.Message);
    }
    
    [Fact]
    public async void LoginAsyncWithUserThatIsLockedOutReturnsErrorAuthResponse() {
        LoginRequestVm request = new() {
            UserNameEmail = "ValidUsername",
            Password = "BadPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser {
                LockoutEnd = DateTime.UtcNow.AddMinutes(15)
            });
        mockAuthUserRepo.Setup(repo => repo.IsEmailConfirmedAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(true);
        mockAuthUserRepo.Setup(repo => repo.AccessFailedAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(IdentityResult.Success);
        mockAuthUserRepo.Setup(repo => repo.CheckPasswordAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(false);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        AuthResponseVm response = await testAuthService.LoginAsync(request);

        Assert.Contains("Error: User has attempted", response.Message);
    }

    [Fact]
    public async void ValidLoginAsyncWithBlankUsernameAndEmailReturnsFilledAuthResponseWithErrorMessage() {
        LoginRequestVm request = new() {
            UserNameEmail = "ValidUsername",
            Password = "GoodPassword_1"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.IsEmailConfirmedAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(true);
        mockAuthUserRepo.Setup(repo => repo.CheckPasswordAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(true);
        mockAuthUserRepo.Setup(repo => repo.ResetAccessFailedCountAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(IdentityResult.Success);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        AuthResponseVm response = await testAuthService.LoginAsync(request);

        Assert.Contains("Error: Email or Username", response.Message);
    }

    [Fact]
    public async void ValidLoginAsyncReturnsFilledAuthResponse() {
        LoginRequestVm request = new() {
            UserNameEmail = "ValidUsername",
            Password = "GoodPassword_1",
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser() {
                UserName = "ValidUsername",
                Email = "ValidPassword@Valid.com",
            });
        mockAuthUserRepo.Setup(repo => repo.IsEmailConfirmedAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(true);
        mockAuthUserRepo.Setup(repo => repo.CheckPasswordAsync(It.IsAny<AuthUser>(), It.IsAny<string>()))
            .ReturnsAsync(true);
        mockAuthUserRepo.Setup(repo => repo.ResetAccessFailedCountAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(IdentityResult.Success);
        mockAuthUserRepo.Setup(repo => repo.GetRolesAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new List<string>() { "User" });

        Mock<IConfiguration> mockConfiguration = new();
        Mock<IConfigurationSection> mockJwtSection = new();
        // ReSharper disable once StringLiteralTypo
        mockJwtSection.Setup(section => section.Value).Returns("27657D6734FBCAF23BF3DD8D7F217");
        mockConfiguration.Setup(config => config.GetSection(It.IsAny<string>()))
            .Returns(mockJwtSection.Object);

        IEnumerable<RefreshToken> refreshTokens = new List<RefreshToken> {
            new() {
                Id = 0,
                Token = "ThisIsAToken.",
                DateAdded = DateTime.UtcNow,
                DateExpired = DateTime.UtcNow.AddMinutes(20)
            }
        }.AsEnumerable();

        Mock<IRefreshTokenRepo> mockRefreshTokenRepo = new();

        mockRefreshTokenRepo.Setup(repo => repo.FilterByUserIdEqual(It.IsAny<int>()))
            .Returns(refreshTokens);
        mockRefreshTokenRepo.Setup(repo => repo.DeleteMultiple(It.IsAny<IEnumerable<RefreshToken>>()))
            .ReturnsAsync(EntityState.Deleted);
        mockRefreshTokenRepo.Setup(repo => repo.InsertAsync(It.IsAny<RefreshToken>()))
            .ReturnsAsync(EntityState.Added);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, mockRefreshTokenRepo.Object,
            mockConfiguration.Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        AuthResponseVm response = await testAuthService.LoginAsync(request);

        Assert.Equal("Token created successfully!", response.Message);
    }

    [Fact]
    public async void LogoutAsyncRunsSuccessfully() {
        LogoutRequestVm request = new() {
            RefreshToken = "ThisIsARefreshToken"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();

        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser() {
                UserName = "ValidUsername",
                Email = "ValidPassword@Valid.com",
            });

        Mock<IRefreshTokenRepo> mockRefreshRepo = new();
        mockRefreshRepo.Setup(repo => repo.DeleteMultiple(It.IsAny<IEnumerable<RefreshToken>>()));
        mockRefreshRepo.Setup(repo => repo.GetTokenAsync(It.IsAny<string>()))
            .ReturnsAsync(new RefreshToken {
                Id = 0,
                Token = "ThisIsARefreshToken",
                DateAdded = DateTime.UtcNow,
                DateExpired = DateTime.UtcNow.AddMinutes(20)
            });

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, mockRefreshRepo.Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        await testAuthService.LogoutAsync(request);
    }

    [Fact]
    public async void ForgotPasswordAsyncWithUsernameOrEmailFilledOutButNoUserFoundReturnsTupleWithError() {
        ForgotPasswordRequestVm request = new() {
            UserNameEmail = "lucinaravenwing"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByEmailAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);
        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        Tuple<string, AuthUser> response = await testAuthService.ForgotPasswordAsync(request);

        Assert.Equal("Error: User not found.", response.Item1);

        request.UserNameEmail = "lucinaravenwing.net@gmail.com";

        response = await testAuthService.ForgotPasswordAsync(request);

        Assert.Equal("Error: User not found.", response.Item1);
    }

    [Fact]
    public async void ValidForgotPasswordReturnsStringWithToken() {
        ForgotPasswordRequestVm request = new() {
            UserNameEmail = "lucinaravenwing"
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByUsernameAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());
        mockAuthUserRepo.Setup(repo => repo.GeneratePasswordResetTokenAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync("ThisIsAResetToken.");

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, new Mock<IRefreshTokenRepo>().Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        Tuple<string, AuthUser> response = await testAuthService.ForgotPasswordAsync(request);

        Assert.Equal("ThisIsAResetToken.", response.Item1);
    }

    [Fact]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public async void
        ValidateAndGenerateTokenAsyncWithUnexpiredTokenAndNonNullUserCallsGenerateTokenAsyncAndErrorsWithEmptyUsernameOrPassword() {
        TokenRequestVm request = new() {
            Token =
                "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg",
            RefreshToken =
                "dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0="
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser());

        Mock<IRefreshTokenRepo> mockRefreshTokenRepo = new();
        mockRefreshTokenRepo.Setup(repo => repo.GetTokenAsync(It.IsAny<string>()))
            .ReturnsAsync(new RefreshToken() {
                Id = 16,
                Token = request.RefreshToken,
                JwtId = "JwtId",
                IsRevoked = false,
                DateAdded = DateTime.UtcNow,
                DateExpired = DateTime.UtcNow.AddMinutes(30)
            });

        SecurityToken? output = new JwtSecurityToken(
            issuer: "Issuer",
            audience: "Audience",
            expires: DateTime.UtcNow.AddMinutes(2),
            signingCredentials: new SigningCredentials(new SymmetricSecurityKey("ThisIsASecurityKey"u8.ToArray()),
                SecurityAlgorithms.HmacSha512)
        );

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, mockRefreshTokenRepo.Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);
        Mock<SecurityTokenHandler> mockTokenHandler = new();
        ClaimsPrincipal claims = new ClaimsPrincipal(new ClaimsIdentity(null, "Basic"));
        mockTokenHandler.Setup(handler =>
                handler.ValidateToken(It.IsAny<string>(), It.IsAny<TokenValidationParameters>(), out output))
            .Returns(claims);

        AuthResponseVm response = await testAuthService.ValidateAndGenerateTokenAsync(mockTokenHandler.Object, request);

        Assert.Contains("Error: Email or Username ", response.Message);
    }

    [Fact]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public async void
        ValidateAndGenerateTokenAsyncWithUnexpiredTokenAndNonNullUserCallsGenerateTokenAsyncAndRunsCorrectly() {
        TokenRequestVm request = new() {
            Token =
                "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg",
            RefreshToken =
                "dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0="
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser() {
                UserName = "lucinaravenwing",
                Email = "lucinaravenwing.net@gmail.com"
            });
        mockAuthUserRepo.Setup(repo => repo.GetRolesAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new List<string>() { "User" });

        Mock<IRefreshTokenRepo> mockRefreshTokenRepo = new();
        mockRefreshTokenRepo.Setup(repo => repo.GetTokenAsync(It.IsAny<string>()))
            .ReturnsAsync(new RefreshToken() {
                Id = 16,
                Token = request.RefreshToken,
                JwtId = "JwtId",
                IsRevoked = false,
                DateAdded = DateTime.UtcNow,
                DateExpired = DateTime.UtcNow.AddMinutes(30)
            });
        mockRefreshTokenRepo.Setup(token => token.InsertAsync(It.IsAny<RefreshToken>()))
            .ReturnsAsync(EntityState.Added);
        mockRefreshTokenRepo.Setup(repo => repo.FilterByUserIdEqual(It.IsAny<int>()))
            .Returns(new List<RefreshToken>() { new() });
        mockRefreshTokenRepo.Setup(repo => repo.DeleteMultiple(It.IsAny<IEnumerable<RefreshToken>>()))
            .ReturnsAsync(EntityState.Deleted);

        SecurityToken? output = new JwtSecurityToken(
            issuer: "Issuer",
            audience: "Audience",
            expires: DateTime.UtcNow.AddMinutes(2),
            signingCredentials: new SigningCredentials(new SymmetricSecurityKey("ThisIsASecurityKey"u8.ToArray()),
                SecurityAlgorithms.HmacSha512)
        );

        Mock<IConfiguration> mockConfiguration = new();
        Mock<IConfigurationSection> mockJwtSection = new();
        // ReSharper disable once StringLiteralTypo
        mockJwtSection.Setup(section => section.Value).Returns("27657D6734FBCAF23BF3DD8D7F217");
        mockConfiguration.Setup(config => config.GetSection(It.IsAny<string>()))
            .Returns(mockJwtSection.Object);

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, mockRefreshTokenRepo.Object,
            mockConfiguration.Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        Mock<SecurityTokenHandler> mockTokenHandler = new();
        ClaimsPrincipal claims = new (new ClaimsIdentity(null, "Basic"));
        mockTokenHandler.Setup(handler =>
                handler.ValidateToken(It.IsAny<string>(), It.IsAny<TokenValidationParameters>(), out output))
            .Returns(claims);

        AuthResponseVm response = await testAuthService.ValidateAndGenerateTokenAsync(mockTokenHandler.Object, request);

        Assert.Contains("Token created successfully!", response.Message);
    }

    [Fact]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public async void ValidateAndGenerateTokenAsyncWithUnexpiredTokenAndNullUserReturnsAuthResponseWithError() {
        TokenRequestVm request = new() {
            Token =
                "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg",
            RefreshToken =
                "dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0="
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        Mock<IRefreshTokenRepo> mockRefreshTokenRepo = new();
        mockRefreshTokenRepo.Setup(repo => repo.GetTokenAsync(It.IsAny<string>()))
            .ReturnsAsync(new RefreshToken() {
                Id = 16,
                Token = request.RefreshToken,
                JwtId = "JwtId",
                IsRevoked = false,
                DateAdded = DateTime.UtcNow,
                DateExpired = DateTime.UtcNow.AddMinutes(30)
            });

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, mockRefreshTokenRepo.Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        AuthResponseVm response =
            await testAuthService.ValidateAndGenerateTokenAsync(new Mock<SecurityTokenHandler>().Object, request);

        Assert.Contains("Error: User associated", response.Message);
    }

    [Fact]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public async void ValidateAndGenerateTokenAsyncWithExpiredOrMissingTokenAndNullUserReturnsError() {
        TokenRequestVm request = new() {
            Token =
                "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg",
            RefreshToken =
                "dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0="
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync((AuthUser?)null);

        Mock<IRefreshTokenRepo> mockRefreshTokenRepo = new();
        mockRefreshTokenRepo.Setup(repo => repo.GetTokenAsync(It.IsAny<string>()))
            .ReturnsAsync(new RefreshToken() {
                Id = 16,
                Token = request.RefreshToken,
                JwtId = "JwtId",
                IsRevoked = false,
                DateAdded = DateTime.UtcNow,
                DateExpired = DateTime.UtcNow.AddMinutes(-30)
            });

        SecurityToken? output = null;

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, mockRefreshTokenRepo.Object,
            new Mock<IConfiguration>().Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        Mock<SecurityTokenHandler> mockTokenHandler = new();
        mockTokenHandler.Setup(handler =>
                handler.ValidateToken(It.IsAny<string>(), It.IsAny<TokenValidationParameters>(), out output))
            .Throws<SecurityTokenException>();

        AuthResponseVm response = await testAuthService.ValidateAndGenerateTokenAsync(mockTokenHandler.Object, request);

        Assert.Contains("Error: User associated", response.Message);
    }

    [Fact]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public async void
        ValidateAndGenerateTokenAsyncWithExpiredTokenAndFoundUserWithUnexpiredRefreshTokenReturnsCurrentRefreshToken() {
        TokenRequestVm request = new() {
            Token =
                "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg",
            RefreshToken =
                "dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0="
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser() {
                UserName = "lucinaravenwing",
                Email = "lucinaravenwing.net@gmail.com"
            });

        Mock<IRefreshTokenRepo> mockRefreshTokenRepo = new();
        mockRefreshTokenRepo.Setup(repo => repo.GetTokenAsync(It.IsAny<string>()))
            .ReturnsAsync(new RefreshToken() {
                Id = 16,
                Token = request.RefreshToken,
                JwtId = "JwtId",
                IsRevoked = false,
                DateAdded = DateTime.UtcNow,
                DateExpired = DateTime.UtcNow.AddMinutes(30)
            });

        SecurityToken? output = null;
        
        Mock<IConfiguration> mockConfiguration = new();
        Mock<IConfigurationSection> mockJwtSection = new();
        // ReSharper disable once StringLiteralTypo
        mockJwtSection.Setup(section => section.Value).Returns("27657D6734FBCAF23BF3DD8D7F217");
        mockConfiguration.Setup(config => config.GetSection(It.IsAny<string>()))
            .Returns(mockJwtSection.Object);

        mockAuthUserRepo.Setup(repo => repo.GetRolesAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new List<string>() { "User" });

        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, mockRefreshTokenRepo.Object,
            mockConfiguration.Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        Mock<SecurityTokenHandler> mockTokenHandler = new();
        mockTokenHandler.Setup(handler =>
                handler.ValidateToken(It.IsAny<string>(), It.IsAny<TokenValidationParameters>(), out output))
            .Throws<SecurityTokenException>();

        AuthResponseVm response = await testAuthService.ValidateAndGenerateTokenAsync(mockTokenHandler.Object, request);

        Assert.Equal(request.RefreshToken, response.RefreshToken);
    }

    [Fact]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    public async void
        ValidateAndGenerateTokenAsyncWithExpiredTokenAndFoundUserWithExpireRefreshTokenReturnsNewRefreshToken() {
        TokenRequestVm request = new() {
            Token =
                "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoidGVzdHVzZXIiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjE2IiwiZW1haWwiOiJ0ZXN0QHRlc3QuY29tIiwic3ViIjoidGVzdHVzZXIiLCJqdGkiOiI3NTAyMjNmZC02MzFhLTRhNWUtODk3NC0zODQ2Nzk4MDMyNmMiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3JvbGUiOiJ1c2VyIiwiZXhwIjoxNjczOTMyNDgyLCJpc3MiOiJ3d3cuYXV0aC5sdWNpbmFyYXZlbndpbmcubmV0IiwiYXVkIjoidXNlciJ9.-mOH_tlGfXsB-Q_ezBDFZhPrUHja1B2y1GLXLqDhO4DgtMZ15SQ8gc4XFrg3uPrDU79bOGcA8_u2HGyZWGGDrg",
            RefreshToken =
                "dBYES7aeoC0DuYzmvDkRFtJXu3AJ8kWbmLH43Ec2Lu6xSg/z339yNdltsmTJ0OGyxleIUbVsFIOFI//0l/g1gAwY0TC9WwbPJ0VBTERqJsrXe212CquhyTw3ouskdR/mjkM79/gb7hPu7A7f4BpIlE1ChSti89dRL6DmSC8+oAl+GWwspcLxTPVzjrzi0Jl9OOGLJAkuJY9jydY3b50oJSFtvu6004R2QWwn5S5QCeizJNx5fVGQLHp+jfpxR1MqkgkPQe7ldworkeRCSYl1pdqjtpOqDS7N72YGFr3SyrEW5A5BnjAVjcj9OXZz/d1X9XwZe0Uq+PVehmu176ssjVUKBEHFsvcpEt21pc9JNh3Yq2XGFrx7e9zZ8/9uH9NlAl2HBrKQgF4EBD56vUA/d3jM3YZm7qc7X9IxzIcrQFFui8uiAS3TuocdoUlRNy/W0WNMeWCWGJvq6aYP/9HerEFy+TaFu+aHGuBSr50J5l6U2XwM1fM6Jx4Ns+X6GZzIkwYRnqZFNtASO6Wp32OZnmvdzIPCvVim7yl1+dUj7usBIEUVs2RlWRb0Y1581X/5AMgemMcfrwpm+ul1+ToREfDHNzkGR3qE59vRaKeOGO2AFFqdARyqHgtMbVwuEWkdKBqbimXVqljR18Q8DFGczOwwPWrQvrpgBLIw0jJX6n0="
        };

        Mock<UserManager<AuthUser>> mockManager = MockUserManagerHelper.MockUserManager<AuthUser>();
        Mock<IAuthUserRepo> mockAuthUserRepo = new();
        mockAuthUserRepo.Setup(repo => repo.FindByIdAsync(It.IsAny<string>()))
            .ReturnsAsync(new AuthUser() {
                UserName = "lucinaravenwing",
                Email = "lucinaravenwing.net@gmail.com"
            });
        mockAuthUserRepo.Setup(repo => repo.GetRolesAsync(It.IsAny<AuthUser>()))
            .ReturnsAsync(new List<string>() { "User" });

        Mock<IRefreshTokenRepo> mockRefreshTokenRepo = new();
        mockRefreshTokenRepo.Setup(repo => repo.GetTokenAsync(It.IsAny<string>()))
            .ReturnsAsync(new RefreshToken() {
                Id = 16,
                Token = request.RefreshToken,
                JwtId = "JwtId",
                IsRevoked = false,
                DateAdded = DateTime.UtcNow,
                DateExpired = DateTime.UtcNow.AddMinutes(-30)
            });

        SecurityToken? output = null;

        Mock<IConfiguration> mockConfiguration = new();
        Mock<IConfigurationSection> mockJwtSection = new();
        // ReSharper disable once StringLiteralTypo
        mockJwtSection.Setup(section => section.Value).Returns("27657D6734FBCAF23BF3DD8D7F217");
        mockConfiguration.Setup(config => config.GetSection(It.IsAny<string>()))
            .Returns(mockJwtSection.Object);
        
        AuthServiceImpl testAuthService = new(mockAuthUserRepo.Object, mockRefreshTokenRepo.Object,
            mockConfiguration.Object, new Mock<TokenValidationParameters>().Object, mockManager.Object);

        Mock<SecurityTokenHandler> mockTokenHandler = new();
        mockTokenHandler.Setup(handler =>
                handler.ValidateToken(It.IsAny<string>(), It.IsAny<TokenValidationParameters>(), out output))
            .Throws<SecurityTokenException>();

        AuthResponseVm response = await testAuthService.ValidateAndGenerateTokenAsync(mockTokenHandler.Object, request);

        Assert.NotEqual(request.RefreshToken, response.RefreshToken);
    }
}