FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build

LABEL maintainer="lucinaravenwing.net@gmail.com"

WORKDIR /home

ADD ./Release/. /Release

FROM mcr.microsoft.com/dotnet/sdk:8.0
WORKDIR /Release

COPY --from=build ./Release/ .

EXPOSE 5000

CMD dotnet --additional-deps ./AuthService.deps.json ./AuthService.dll