# Auth Services - ASP.NET

[![Build Status](https://jenkins.lucinaravenwing.net/buildStatus/icon?job=auth-service-asp&style=plastic)](https://jenkins.lucinaravenwing.net/job/auth-service-asp/)

## Repository link
https://gitlab.com/lucina.ravenwing/authservice

## URL
https://auth.lucinaravenwing.net/api/swagger/v1/index.html

## Description
Authentication services for lucinaravenwing.org sites. Will authenticate the user and return a token that will allow the user to use a single account for all different applications under the suite. ASP.NET version.

## Languages
C#

## Resources Used
ASP.NET\
Entity Framework\
xUnit\
PostgreSQL\
Docker\
nginx\
Rest API\
Jenkins

==================