pipeline {
    agent { label 'auth' }
    stages {
        stage('build') {
            steps {
                echo 'Starting build stage.'
                updateGitlabCommitStatus name: 'build', state: 'pending'
                sh 'cd /home/jenkins/workspace/auth-service-asp'
                sh 'cp /home/jenkins/appsettings.json ./AuthService; cp /home/jenkins/appsettings.Development.json ./AuthService'
                sh 'cp /home/jenkins/nlog.config ./AuthService'
                sh 'dotnet build'
            }
        }
        stage('test') {
            steps {
                echo 'Starting test stage.'
                sh 'dotnet test ./AuthServiceTest/bin/Debug/net7.0/AuthServiceTest.dll'
            }
        }
        stage('release') {
            steps {
                echo 'Starting release stage. Includes:\n\t- Archive the old release folder.\n\t- Publish the new release folder. \n\t- Build the docker image locally.\n\t- Test that the deploy worked (WIP).\n\t- Push the image to Docker Hub.'

                sh 'dotnet publish ./AuthService/AuthService.csproj -c RELEASE -o ./Release'

                // Archive old jar and move new jar to correct folder
                sh 'rm -r /home/jenkins/archived-asp/.net7 || true'
                sh 'mv /home/jenkins/deploy-asp/* /home/jenkins/archived-asp || true'
//                 sh 'ls; ls ./AuthService; ls ./AuthService/bin; ls ./AuthService/bin/Release'
                sh 'cp -r ./AuthService/bin/RELEASE/* /home/jenkins/deploy-asp'

                // Docker deploy steps
                sh 'docker stop auth-service-asp || true'
                sh 'docker rm auth-service-asp || true'

                // Push image to docker Hub
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'jenkins-dockerhub') {
                        app = docker.build("lucinaravenwing/auth-service-asp")
                        app.push("${env.BUILD_NUMBER}")
                        app.push("latest")
                    }
                }
            }
        }
        stage('deploy') {
            steps {
                echo 'Starting deploy stage. Includes:\n\t- Running the docker container.'
                sh 'docker run -d -it --network=host --name=auth-service-asp -v logs:/logs lucinaravenwing/auth-service-asp'
                updateGitlabCommitStatus name: 'build', state: 'success'
            }
        }
    }
}
